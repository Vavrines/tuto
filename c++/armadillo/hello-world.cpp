// g++ hello-world.cpp -o hello -std=c++11 -O2 -larmadillo

#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main()
{
    mat A(4, 5, fill::randu);
    mat B(4, 5, fill::randu);
    cout << A * B.t() << endl;

    mat C = randn(2, 3);
    cout << C << endl;

    return 0;
}