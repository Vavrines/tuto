# https://ucl-bug.github.io/jaxdf/notebooks/pinn_burgers.html
# Note that it is much easier to converge solution with viscosity

# %%
import jax
from jax import numpy as jnp
from jax import random
from jax.example_libraries import stax, optimizers
from jaxdf.geometry import Domain
from jaxdf.discretization import Continuous
from jaxdf.operators.differential import derivative
from functools import partial
from tqdm import tqdm
from matplotlib import pyplot as plt

# %%
nx, nt = 200, 1000
dx, dt = 0.01, 0.001
seed = random.PRNGKey(0)
width, depth = 20, 9
batchsize = 256

# %%
domain = Domain(N=(nx, nt), dx=(dx, dt))  # space, time
domain_sampler = domain.domain_sampler
boundary_sampler = domain.boundary_sampler

# test
x = boundary_sampler(random.PRNGKey(42), 128)
plt.scatter(x[:, 1], x[:, 0], marker="x", label="boundary")
x = domain_sampler(random.PRNGKey(42), 128)
plt.scatter(x[:, 1], x[:, 0], marker="o", label="domain")
plt.legend()
plt.title("Provided samplers")
plt.show()

# %%
fn_act = stax.elementwise(jnp.tanh)
layers = [stax.Dense(width), fn_act] * depth
init_random_params, forward = stax.serial(*layers, stax.Dense(1))

def init_params(seed, domain):
    return init_random_params(seed, (len(domain.N),))[1]

def get_fun(params, x):
    return forward(params, x)

# test
input_shape = (-1, 2) # (batch_size, input_dim)
output_shape, params = init_random_params(seed, input_shape)
forward(params, jnp.array([0.0, 0.0]))

u0 = Continuous.from_function(domain, init_params, get_fun, seed)
u0(jnp.array([0.0, 0.0]))

# %%
def burgers(u):
    du_dt = derivative(u, axis=1)
    du_dx = derivative(u, axis=0)
    ddu_dx = derivative(du_dx, axis=0)
    return du_dt + u * du_dx + (-0.01 / jnp.pi) * ddu_dx

@partial(jax.jit, static_argnums=2)
def boundary_loss(u, seed, batchsize):
    x = domain_sampler(seed, batchsize)

    # initial condition
    @jax.vmap
    def field_fun(x):
        return u.get_field(x)

    y = x.at[:, 1].set(-0.5) # t ∈ [-0.5, 0.5] in domain
    field_val = field_fun(y)
    target_val = jnp.expand_dims(-jnp.sin(jnp.pi * x[:, 0]), -1)
    l1 = jnp.mean(jnp.abs(target_val - field_val) ** 2)

    # boundary condition
    y = x.at[:, 0].set(random.bernoulli(seed, shape=(batchsize,)) * 2 - 1)
    field_val = field_fun(y)
    target_val = 0
    l2 = jnp.mean(jnp.abs(target_val - field_val) ** 2)

    return (l1 + l2) / 2

@partial(jax.jit, static_argnums=2)
def domain_loss(u, seed, batchsize):
    x = domain_sampler(seed, batchsize)

    @jax.vmap
    def residual_fun(x):
        return burgers(u)(x)

    residual = residual_fun(x)
    return jnp.mean(jnp.abs(residual) ** 2)

@jax.jit
def update(opt_state, seed, k):
    seeds = random.split(seed, 2)

    def tot_loss(u):
        l1 = domain_loss(u, seeds[0], batchsize)
        l2 = boundary_loss(u, seeds[1], batchsize)
        return l1 + l2

    lossval, grads = jax.value_and_grad(tot_loss)(get_params(opt_state))

    return lossval, update_fun(k, grads, opt_state)

# %%
u = Continuous.from_function(domain, init_params, get_fun, seed)
init_fun, update_fun, get_params = optimizers.adam(0.0001)
opt_state = init_fun(u)

# test
bl = boundary_loss(u, seed, 32)
dl = domain_loss(u, seed, 32)
print(f"Domain loss: {dl.item()}, Boundary loss: {bl.item()}")

# %%
pbar = tqdm(range(80000))
for k in pbar:
    rng, seed = random.split(seed, 2)
    lossval, opt_state = update(opt_state, seed, k)

    if k % 500 == 99:
        pbar.set_description(f"Loss: {lossval}")

# %%
def show_field(u):
    plt.figure(figsize=(15, 4))
    plt.imshow(
        u.on_grid, cmap="RdBu", extent=[0, 1, -1, 1], vmin=-1.0, vmax=1.0, aspect="auto"
    ) # note that this extend is not the real domain of t
    plt.xlabel("$t$")
    plt.ylabel("$x$")
    plt.colorbar()
    plt.show()

u = get_params(opt_state)
show_field(u)

# %%
