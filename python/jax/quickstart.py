# %%
import numpy as np
import jax.numpy as jnp

def selu(x, alpha=1.67, lmbda=1.05):
    return lmbda * jnp.where(x > 0, x, alpha * jnp.exp(x) - alpha)

# %%
from jax import random
x = random.normal(random.key(1701), 1_000_000)
x0 = np.random.randn(1_000_000)

# %%
from jax import jit

selu_jit = jit(selu)
_ = selu_jit(x)
%timeit selu_jit(x).block_until_ready()
# ~57.2 µs ± 833 ns per loop

# %%
%timeit selu(x0).block_until_ready()
# ~2.79 ms ± 46.7 µs per loop

# %%
from jax import grad

def sum_logistic(x):
    return jnp.sum(1.0 / (1.0 + jnp.exp(-x)))

x_small = jnp.arange(3.)
derivative_fn = grad(sum_logistic)
derivative_fn(x_small)

# %%
from jax import jacobian
jacobian(jnp.exp)(x_small)

# %%
key = random.key(1701)
key1, key2 = random.split(key)
mat = random.normal(key1, (150, 100))
batched_x = random.normal(key2, (10, 100))

def apply_matrix(x):
    return jnp.dot(mat, x)

%timeit jnp.stack([apply_matrix(v) for v in batched_x])
# ~1.55 ms ± 5.37 µs per loop

# %%
from jax import vmap

@jit
def vmap_batched_apply_matrix(batched_x):
    return vmap(apply_matrix)(batched_x)

%timeit vmap_batched_apply_matrix(batched_x).block_until_ready()
# 70.3 µs ± 768 ns per loop
