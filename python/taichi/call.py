import sys
sys.path.insert(0, "/home2/vavrines/Coding/tuto/python/taichi")

import numpy as np
u = np.linspace(-5, 5, 100)
prim = np.array((1.0, 0.0, 1.0))
M = np.zeros(100)

import kit
kit.maxwellian(M, u, prim)

print(M)
