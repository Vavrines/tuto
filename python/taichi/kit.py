import taichi as ti
import taichi.math as tm
import numpy as np

ti.init(arch=ti.cpu)
# ti.init(arch=ti.gpu)


@ti.kernel
def maxwellian(M: ti.types.ndarray(), u: ti.types.ndarray(), prim: ti.types.ndarray()):
    for i in range(u.shape[0]):
        M[i] = prim[0] * (prim[2] / tm.pi)**0.5 * \
            tm.exp(-prim[2] * (u[i] - prim[1])**2)


def maxwellian_np(M, u, prim):
    for i in range(u.shape[0]):
        M[i] = prim[0] * (prim[2] / np.pi)**0.5 * \
            np.exp(-prim[2] * (u[i] - prim[1])**2)
