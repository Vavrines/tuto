using PyCall
using BenchmarkTools
using Base.Threads: @threads

py"""
import sys
sys.path.insert(0, "/home2/vavrines/Coding/tuto/python/taichi")
"""

KT = pyimport("kit")

u = -5:0.01:5 |> collect
prim = [1., 0, 1.]
M = zero(u)

@btime KT.maxwellian($M, $u, $prim)
@btime KT.maxwellian_np($M, $u, $prim)

function maxwellian_jl(H, u, prim)
    @threads for i in eachindex(H)
        H[i] = prim[1] * (prim[3] / pi)^0.5 * exp(-prim[3] * (u[i] - prim[2])^2)
    end
    return nothing
end

@btime maxwellian_jl($M, $u, $prim)

using Plots
plot(u, M)
