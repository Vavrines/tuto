from matplotlib import pyplot as plt
import taichi as ti
import taichi.math as tm
import numpy as np
from time import process_time

ti.init(arch=ti.cpu)


def solver(u, u0, c: float, dx: float, dt: float, t_max: float):
    # Compute the number of grid points and time steps
    nx = u0.size
    nt = int(t_max / dt)

    # Create the solution array
    u[0, :] = u0

    # Solve the equation at each time step
    for t in range(1, nt):
        for i in range(1, nx):
            # Use the finite difference method to compute the solution
            u[t, i] = u[t-1, i] - c * dt / dx * (u[t-1, i] - u[t-1, i-1])


@ti.kernel
def solver_ti(u: ti.types.ndarray(), u0: ti.types.ndarray(), c: float, dx: float, dt: float, t_max: float):
    # Compute the number of grid points and time steps
    nx = 101  # u0.size
    nt = int(t_max / dt)

    # Create the solution array
    for i in range(1, nx):
        u[0, i] = u0[i]

    # Solve the equation at each time step
    for t in range(1, nt):
        for i in range(1, nx):
            # Use the finite difference method to compute the solution
            u[t, i] = u[t-1, i] - c * dt / dx * (u[t-1, i] - u[t-1, i-1])


# Set the parameters
c = 1.0  # Advection speed
dx = 0.01  # Spatial grid size
dt = 0.01  # Time step size
t_max = 0.1  # Maximum time to solve the equation for
nt = int(t_max / dt)
u0 = np.exp(-100 * (np.linspace(0, 1, 101) - 0.5) ** 2)  # Initial condition

u = np.zeros((nt, 101))
t0 = process_time()
solver(u, u0, c, dx, dt, t_max)
t1 = process_time()
print(t1-t0)

u = np.zeros((nt, 101))
t0 = process_time()
solver_ti(u, u0, c, dx, dt, t_max)
t1 = process_time()
print(t1-t0)


plt.plot(u[-1, :])
plt.plot(u0)
plt.show()
