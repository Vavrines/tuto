#%%
import numpy as np
import matplotlib.pyplot as plt
from pyapprox.benchmarks import setup_benchmark
from pyapprox import interface
from pyapprox import multifidelity

#%%
np.random.seed(1)
benchmark = setup_benchmark("short_column_ensemble")
short_column_model = benchmark.fun
model_costs = np.asarray([100, 50, 5, 1, 0.2])

#%%
target_cost = int(1e4)
idx = [0, 1]
cov = short_column_model.get_covariance_matrix()[np.ix_(idx, idx)]
model_ensemble = interface.ModelEnsemble(
    [short_column_model.models[ii] for ii in idx])
costs = model_costs[idx]

#%%
est = multifidelity.get_estimator("mlmc", cov, costs, benchmark.variable)
nsample_ratios, variance, rounded_target_cost = est.allocate_samples(
    target_cost)
acv_samples, acv_values = est.generate_data(model_ensemble)
mlmc_mean = est(acv_values)
hf_mean = acv_values[0][1].mean()

#%%
true_mean = short_column_model.get_means()[0]
print('MC error', abs(hf_mean-true_mean))
print('MLMC error', abs(mlmc_mean-true_mean))

# %%
