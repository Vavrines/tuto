# %%
import dapper as dpr
import dapper.da_methods as da

# Load experiment setup: the hidden Markov model (HMM)
from dapper.mods.Lorenz63.sakov2012 import HMM
HMM.tseq.T = 30  # shorten experiment

# Simulate synthetic truth (xx) and noisy obs (yy)
xx, yy = HMM.simulate()

# Specify a DA method configuration ("xp" is short for "experiment")
# xp = da.OptInterp()
# xp = da.Var3D()
# xp = da.ExtKF(infl=90)
xp = da.EnKF('Sqrt', N=10, infl=1.02, rot=True)
# xp = da.PartFilt(N=100, reg=2.4, NER=0.3)

# Assimilate yy, knowing the HMM; xx is used to assess the performance
xp.assimilate(HMM, xx, yy)

# #### Average the time series of various statistics
# print(xp.stats)  # ⇒ long printout
xp.stats.average_in_time()

print(xp.avrgs.tabulate(['rmse.a', 'rmv.a']))
# %%
xp.stats.replay()
# %%
import dapper.tools.viz as viz
viz.plot_rank_histogram(xp.stats)
viz.plot_err_components(xp.stats)
viz.plot_hovmoller(xx)
# %%
