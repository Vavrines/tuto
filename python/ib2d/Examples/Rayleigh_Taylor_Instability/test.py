import numpy as np
import os
os.chdir('/home/vavrines/Coding/tuto/python/ib2d/Examples/Rayleigh_Taylor_Instability')
import sys
sys.path.append('../../IBM_Blackbox')
import IBM_Driver as Driver
from please_Initialize_Simulation import please_Initialize_Simulation

Fluid_Params, Grid_Params, Time_Params, Lag_Struct_Params, Output_Params, Lag_Name_Params = please_Initialize_Simulation()

struct_name = Lag_Name_Params

# FLUID PARAMETER VALUES STORED #
mu = Fluid_Params[0]      # Dynamic Viscosity
rho = Fluid_Params[1]     # Density

# TEMPORAL INFORMATION VALUES STORED #
T_FINAL = Time_Params[0]     # Final simulation time
dt = Time_Params[1]          # Time-step
#NTime = floor[T_FINAL/dt]+1 # # of total time-steps [floor'd so exact number of time-steps]
#dt = T_FINAL/NTime          # revised time-step [slightly perturbed dt, so exact # of time-steps are used]
current_time = 0.0           # initialize start of simulation to time, 0 


# GRID INFO #
Nx =   int(Grid_Params[0])               # # of Eulerian pts. in x-direction
Ny =   int(Grid_Params[1])                # # of Eulerian pts. in y-direction
Lx =   Grid_Params[2]                # Length of Eulerian grid in x-coordinate
Ly =   Grid_Params[3]                # Length of Eulerian grid in y-coordinate
dx =   Grid_Params[2]/Grid_Params[0] # Spatial-size in x
dy =   Grid_Params[3]/Grid_Params[1] # Spatial-size in y
supp = Grid_Params[4]                # Delta-function support
grid_Info = [Nx,Ny,Lx,Ly,dx,dy,supp] # Store for passing values into IB time-loop sub-functions
                                    # NOTE: grid_Info[8] = Nb, grid_Info[9] = ds [THEY GET STORED LATER]

# PRINTING/PLOTTING INFO #
pDump = Output_Params[0]          # Print [Plot] Dump interval
pMatplotlib = Output_Params[1]    # Plot in Matlab? [1=YES,0=NO]
lagPlot = Output_Params[2]        # Plot LAGRANGIAN PTs ONLY in Matlab
velPlot = Output_Params[3]        # Plot LAGRANGIAN PTs + VELOCITY FIELD in Matlab
vortPlot = Output_Params[4]       # Plot LAGRANGIAN PTs + VORTICITY colormap in Matlab
uMagPlot = Output_Params[5]       # Plot LAGRANGIAN PTs + MAGNITUDE OF VELOCITY colormap in Matlab
pressPlot = Output_Params[6]      # Plot LAGRANGIAN PTs + PRESSURE colormap in Matlab


# MODEL STRUCTURE DATA STORED #
springs_Yes = Lag_Struct_Params[0]           # Springs: 0 [for no] or 1 [for yes] 
update_Springs_Flag = Lag_Struct_Params[1]   # Update_Springs: 0 [for no] or 1 [for yes]
target_pts_Yes = Lag_Struct_Params[2]        # Target_Pts: 0 [for no] or 1 [for yes]
update_Target_Pts = Lag_Struct_Params[3]     # Update_Target_Pts: 0 [for no] or 1 [for yes]
beams_Yes = Lag_Struct_Params[4]             # Beams: 0 [for no] or 1 [for yes]
update_Beams_Flag = Lag_Struct_Params[5]     # Update_Beams: 0 [for no] or 1 [for yes]
nonInv_beams_Yes = Lag_Struct_Params[6]      # Beams [non-invariant]: 0 [for no] or 1 [for yes]
update_nonInv_Beams_Flag = Lag_Struct_Params[7] # Update_nonInv_Beams: 0 [for no] or 1 [for yes]
muscles_Yes = Lag_Struct_Params[8]           # FV-LT Muscles: 0 [for no] or 1 [for yes]
hill_3_muscles_Yes = Lag_Struct_Params[9]    # Hill 3-Element Muscle: 0 [for no] or 1 [for yes]
arb_ext_force_Yes = Lag_Struct_Params[10]     # Arbitrary External Force: 0 [for no] or 1 [for yes]
tracers_Yes = Lag_Struct_Params[11]          # Tracers: 0 [for no] or 1 [for yes]
mass_Yes = Lag_Struct_Params[12]             # Mass Points: 0 [for no] or 1 [for yes]
gravity_Yes = Lag_Struct_Params[13]          # Gravity: 0 [for no] or 1 [for yes]
# NOTE: Lag_Struct_Params[14],[15]:            <- components of gravity vector [if gravity, initialize them below]
porous_Yes = Lag_Struct_Params[16]           # Porous Media: 0 [for no] or 1 [for yes]
concentration_Yes = Lag_Struct_Params[17]    # Background Concentration Gradient: 0 [for no] or 1 [for yes]
electro_phys_Yes = Lag_Struct_Params[18]     # Electrophysiology [FitzHugh-Nagumo]: 0 [for no] or 1 [for yes]
d_Springs_Yes = Lag_Struct_Params[19]        # Damped Springs: 0 [for no] or 1 [for yes]
update_D_Springs_Flag = Lag_Struct_Params[20]# Update_Damped_Springs: # 0 [for no] or 1 [for yes]
boussinesq_Yes = Lag_Struct_Params[21]       # Boussinesq Approx.: 0 [for no] or 1 [for yes]
exp_Coeff = Lag_Struct_Params[22]            # Expansion Coefficient [e.g., thermal, etc] for Boussinesq approx.
general_force_Yes = Lag_Struct_Params[23]    # General User-Defined Force Term: 0 [for no] or 1 [for yes]
poroelastic_Yes = Lag_Struct_Params[24]      # Poroelastic media 0 [for no] or 1 [for yes]
brinkman_Yes = Lag_Struct_Params[25]         # Brinkman term in Momentum equation 0 [for no] or 1 [for yes]

print(Lag_Struct_Params)

# CLEAR INPUT DATA #


# Temporal Information
#NTime = np.floor(T_FINAL/dt)+1 # number of total time-steps,
                            # (floored, so exact number of time-steps)
#dt = T_FINAL/NTime #time-step (slightly perturbed dt, so exact number of 
                #time-steps are used
#current_time = 0.0







# Create EULERIAN Mesh (these assume periodicity in x and y)
x = np.arange(0,Lx,dx)
y = np.arange(0,Ly,dy)
# Create x-Mesh
#X = np.empty((Nx,x.size))
#for ii in range(Nx):
#    X[ii,] = x
# Create y-Mesh
#Y = np.empty((y.size,Ny))
#for ii in range(Ny):
#    Y[:,ii] = y
# MATLAB SYNTAX: [X,Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy)
X,Y = np.meshgrid(x,y)
# MATLAB SYNTAX: [idX,idY] = meshgrid(0:Nx-1,0:Ny-1)  <--- INITIALIZE FOR FLUID SOLVER FFT FUNCTION    
idX,idY = np.meshgrid(np.arange(0,Nx,1),np.arange(0,Ny,1)) # <- INITIALIZES FOR FLUID SOLVER FFT OPERATORS

# # # # # HOPEFULLY WHERE I CAN READ IN INFO!!! # # # # #


# READ IN LAGRANGIAN POINTS #
Nb,xLag,yLag = read_Vertex_Points(struct_name)
grid_Info.append(Nb)       # Total Number of Lagrangian Pts. (grid_Info[7]=Nb)
xLag_P = xLag              # Initialize previous Lagrangian x-Values 
                        #   (for use in muscle-model)
yLag_P = yLag              # Initialize previous Lagrangian y-Values 
                        #   (for use in muscle-model)

#Lagrangian Structure Data
ds = Lx/(2.*Nx)             # Lagrangian Spacing
grid_Info.append(ds)        # grid_Info[8] = ds                  

print('\n--> FIBER MODEL INCLUDES: \n')
            
                        




    
    
# READ IN SPRINGS (IF THERE ARE SPRINGS) #
if springs_Yes:
    print('  - Springs and ...')
    if update_Springs_Flag == 0:
        print('                   NOT dynamically updating spring properties\n')
    else:
        print('                   dynamically updating spring properties\n')

    springs_info = read_Spring_Points(struct_name)
        #springs_info: col 1: starting spring pt (by lag. discretization)
        #              col 2: ending spring pt. (by lag. discretization)
        #              col 3: spring stiffness
        #              col 4: spring resting lengths
        #              col 5: degree of non-linearity (1=linear)
else:
    springs_info = np.zeros((1,1))  #just to pass placeholder into 
        # "please_Find_Lagrangian_Forces_On_Eulerian_grid function"



# READ IN BEAMS (IF THERE ARE TORSIONAL SPRINGS aka BEAMS) #
if beams_Yes:
    print('  - Beams ("Torsional Springs") and ... ')
    if update_Beams_Flag == 0:
        print('                    NOT dynamically updating beam properties\n')
    else:
        print('                    dynamically updating beam properties\n')

    beams_info = read_Beam_Points(struct_name)
    #beams:      col 1: 1ST PT.
    #            col 2: MIDDLE PT. (where force is exerted)
    #            col 3: 3RD PT.
    #            col 4: beam stiffness
    #            col 5: curavture
else:
    beams_info = 0



# READ IN NON-INVARIANT BEAMS (IF THERE ARE NON-INVARIANT BEAMS) #
if nonInv_beams_Yes:
    print('  - Beams ("Non-Invariant Beams") and ... ')
    if update_Beams_Flag == 0:
        print('                    NOT dynamically updating beam properties\n')
    else:
        print('                    dynamically updating beam properties\n')

    nonInv_beams_info = read_nonInv_Beam_Points(struct_name)
    #beams:      col 1: 1ST PT.
    #            col 2: MIDDLE PT. (where force is exerted)
    #            col 3: 3RD PT.
    #            col 4: beam stiffness
    #            col 5: x-curavture
    #            col 6: y-curvature
else:
    nonInv_beams_info = 0    


# READ IN DAMPED SPRINGS (IF THERE ARE DAMPED SPRINGS) #
if d_Springs_Yes:
    print('  - Damped Springs and ...')
    if update_D_Springs_Flag == 0:
        print('                   NOT dynamically updating damped spring properties\n')
    else:
        print('                   dynamically updating spring properties\n')

    d_springs_info = read_Damped_Spring_Points(struct_name) 
        #springs_info: col 1: starting spring pt (by lag. discretization)
        #              col 2: ending spring pt. (by lag. discretization)
        #              col 3: spring stiffness
        #              col 4: spring resting lengths
        #              col 5: damping coefficient
else:
    d_springs_info = np.zeros((1,1))  #just to pass placeholder into 
        # "please_Find_Lagrangian_Forces_On_Eulerian_grid function"



# READ IN TARGET POINTS (IF THERE ARE TARGET PTS) #
if target_pts_Yes:
    print('  - Target Pts. and ...')
    if update_Target_Pts == 0:
        print('                 NOT dynamically updating target point properties\n')
    else:
        print('                 dynamically updating target point properties\n')
    
    target_aux = read_Target_Points(struct_name)
    #target_aux: col 0: Lag Pt. ID w/ Associated Target Pt.
    #            col 1: target STIFFNESSES
    
    # initialize target_info
    target_info = np.empty((target_aux.shape[0],4))
    
    target_info[:,0] = target_aux[:,0] #Stores Lag-Pt IDs in col vector
    # Stores Original x-Lags and y-Lags as x/y-Target Pt. Identities
    target_info[:,1] = xLag[target_info[:,0].astype('int')]
    target_info[:,2] = yLag[target_info[:,0].astype('int')]
    
    target_info[:,3] = target_aux[:,1] #Stores Target Stiffnesses 
else:
    target_info = np.zeros((1,1))






# READ IN MASS POINTS (IF THERE ARE MASS PTS) #
if mass_Yes:
    print('  - Mass Pts. with ')
    if gravity_Yes == 0:
        print('          NO artificial gravity\n')
    else:
        print('          artificial gravity\n')

    mass_aux = read_Mass_Points(struct_name)
    #target_aux: col 0: Lag Pt. ID w/ Associated Mass Pt.
    #            col 1: "Mass-spring" stiffness parameter
    #            col 2: "MASS" value parameter
    
    if ( mass_aux.size/3.0 < 2.0 ):  # checks if only 1 mass point

        # initialize mass_info
        mass_info = np.empty((1,5))
    
        mass_info[0,0] = mass_aux[0] #Stores Lag-Pt IDs in col vector
        
        #Stores Original x-Lags and y-Lags as x/y-Mass Pt. Identities
        if ( np.isscalar(xLag) ):
            mass_info[0,1] = xLag
            mass_info[0,2] = yLag
        else:
            mass_info[0,1] = xLag[mass_info[0].astype('int')]
            mass_info[0,2] = yLag[mass_info[0].astype('int')]

        mass_info[0,3] = mass_aux[1]   #Stores "mass-spring" parameter 
        mass_info[0,4] = mass_aux[2]   #Stores "MASS" value parameter
    
    else:
        # initialize mass_info
        mass_info = np.empty((mass_aux.shape[0],5))
    
        mass_info[:,0] = mass_aux[:,0] #Stores Lag-Pt IDs in col vector
        #Stores Original x-Lags and y-Lags as x/y-Mass Pt. Identities
        mass_info[:,1] = xLag[mass_info[:,0].astype('int')]
        mass_info[:,2] = yLag[mass_info[:,0].astype('int')]
    
        mass_info[:,3] = mass_aux[:,1]   #Stores "mass-spring" parameter 
        mass_info[:,4] = mass_aux[:,2]   #Stores "MASS" value parameter


else:
    mass_info = np.zeros((1,1))







# READ IN POROUS MEDIA INFO (IF THERE IS POROSITY) #
if porous_Yes:
    print('  - Porous Points\n')
    porous_aux = read_Porous_Points(struct_name)
    #porous_aux: col 1: Lag Pt. ID w/ Associated Porous Pt.
    #            col 2: Porosity coefficient
    #            col 3: Porous Pt. 'Stencil' flag for derivative
    
    # initizlize porous_info
    porous_info = np.empty((porous_aux.shape[0],5))
    
    porous_info[:,0] = porous_aux[:,0] #Stores Lag-Pt IDs in col vector
    # Stores Original x-Lags and y-Lags as x/y-Porous Pt. Identities
    porous_info[:,1] = xLag[porous_info[:,0].astype('int')]
    porous_info[:,2] = yLag[porous_info[:,0].astype('int')]
    
    porous_info[:,3] = porous_aux[:,1] # Stores Porosity Coefficient 
    porous_info[:,4] = porous_aux[:,2] # Stores Porosity Stencil Flag
else:
    porous_info = np.zeros((1,1))



# READ IN PORO-ELASTIC MEDIA INFO (IF THERE IS PORO-ELASTICITY) #
if ( poroelastic_Yes ):
    print('  -Poroelastic media\n')
    poroelastic_info = read_PoroElastic_Points(struct_name)
    F_Poro = np.zeros( ( len(poroelastic_info), 2) )   # Initialization
    #poroelastic_info: col 1: Lag Pt. ID w/ Associated Porous Pt.
    #                  col 2: Porosity coefficient
else:
    poroelastic_info = np.zeros((1,1))
    F_Poro = np.zeros((1,1))




# READ IN MUSCLES (IF THERE ARE MUSCLES) #
if muscles_Yes:
    print('  - MUSCLE MODEL (Force-Velocity / Length-Tension Model)\n')
    muscles_info = read_Muscle_Points(struct_name)
        #         muscles: col 1: MASTER NODE (by lag. discretization)
        #         col 2: SLAVE NODE (by lag. discretization)
        #         col 3: length for max. muscle tension
        #         col 4: muscle constant
        #         col 5: hill parameter, a
        #         col 6: hill parameters, b
        #         col 7: force maximum!
else:
    muscles_info = np.zeros((1,1))  #just to pass placeholder into 
        # "please_Find_Lagrangian_Forces_On_Eulerian_grid function"



# READ IN MUSCLES (IF THERE ARE MUSCLES) #
if hill_3_muscles_Yes:
    print('  - MUSCLE MODEL (3 Element Hill Model)\n')
    muscles3_info = read_Hill_3Muscle_Points(struct_name)
        #         muscles: col 1: MASTER NODE (by lag. discretization)
        #         col 2: SLAVE NODE (by lag. discretization)
        #         col 3: length for max. muscle tension
        #         col 4: muscle constant
        #         col 5: hill parameter, a
        #         col 6: hill parameters, b
        #         col 7: force maximum!
else:
    muscles3_info = np.zeros((1,1))  #just to pass placeholder into "please_Find_Lagrangian_Forces_On_Eulerian_grid function"



# READ IN USER-DEFINED FORCE MODEL PARAMETERS (IF THERE IS A USER-DEFINED FORCE) #
if general_force_Yes:
    print('  - GENERAL FORCE MODEL (user-defined force term)\n')
    gen_force_info = read_General_Forcing_Function(struct_name)
    #
    #           
    #   ALL PARAMETERS / FORCE FUNCTION SET BY USER!            
    #            
    #            
else:
    gen_force_info = 0  

    

# CONSTRUCT GRAVITY INFORMATION (IF THERE IS GRAVITY) #
if gravity_Yes:    
    xG = Lag_Struct_Params[14]      # x-Component of Gravity Vector
    yG = Lag_Struct_Params[15]      # y-Component of Gravity Vector
    normG = sqrt( xG**2 + yG**2 )
    gravity_Info = [gravity_Yes, xG/normG, yG/normG]
    #   col 1: flag if considering gravity
    #   col 2: x-component of gravity vector (normalized)
    #   col 3: y-component of gravity vector (normalized)
    
    del xG, yG, normG
    
else:
    gravity_Info = np.zeros((1,1))


#
# BACKGROUND FLOW ITEMS
#
print('\n\n--> Background Flow Items\n')
if ( tracers_Yes == 0 ) and (concentration_Yes == 0):
    print('      (No tracers nor other passive scalars immersed in fluid)\n\n')


# READ IN TRACERS (IF THERE ARE TRACERS) #
if tracers_Yes:
    print('  -Tracer Particles included\n')
    nulvar,xT,yT = read_Tracer_Points(struct_name)
    tracers = np.zeros((xT.size,4))
    tracers[0,0] = 1
    tracers[:,1] = xT
    tracers[:,2] = yT
        #tracers_info: col 1: xPt of Tracers
        #              col 2: yPt of Tracers
else:
    tracers = np.zeros((1,1))



# READ IN CONCENTRATION (IF THERE IS A BACKGROUND CONCENTRATION) #
if concentration_Yes:
    print('  -Background concentration included\n')
    C,kDiffusion = read_In_Concentration_Info(struct_name)
        #C:           Initial background concentration
        #kDiffusion:  Diffusion constant for Advection-Diffusion
else:
    C = 0 # placeholder for plotting 



# READ IN BRINKMAN TERM (IF THERE IS A BRINKMAN TERM) #
if brinkman_Yes:
    print('  -Brinkman term included\n')
    Brink,kBrink = read_In_Brinkman_Info(struct_name)
        #Brink:   Initial brinkman permeability matrix
        #kBrink:  Permeability for brinkman model
else:
    Brink = 0 # placeholder for plotting 


# CONSTRUCT BOUSSINESQ INFORMATION (IF USING BOUSSINESQ) #
if boussinesq_Yes:
    print('  -Boussinesq Approximation included\n')
    print('     -> NEED: 1. gravity flag w/ gravity components \n')
    print('              2. background concentration \n')

    if exp_Coeff == 0:
        print('     -> exp_Coeff set to 1.0 by default, was assigned 0 in input2d <-\n')
        exp_Coeff = 1.0

    #if gravity_Info == 0:
    #    print('\n\n\n READ THE ERROR MESSAGE -> YOU MUST FLAG GRAVITY IN INPUT FILE FOR BOUSSINESQ! :) \n\n\n')
    #    error('YOU MUST FLAG GRAVITY IN INPUT FILE FOR BOUSSINESQ! :)')
    #elif concentration_Yes == 0:
    #    print('\n\n\n READ THE ERROR MESSAGE -> YOU MUST HAVE BACKGROUND CONCENTRATION FOR BOUSSINESQ! :) \n\n\n')
    #    error('YOU MUST FLAG CONCENTRATION IN INPUT FILE FOR BOUSSINESQ! :)')

    # Forms Boussinesq forcing terms, e.g., (exp_Coeff)*gVector for Momentum Eq.
    fBouss_X,fBouss_Y = please_Form_Boussinesq_Forcing_Terms(exp_Coeff,Nx,Ny,gravity_Info)

    # Finds initial concentration Laplacian (may not be necessary)
    #Cxx = DD(C,dx,'x')
    #Cyy = DD(C,dy,'y')
    #laplacian_C = Cxx+Cyy
    #C_0 = np.zeros(C.shape) # Define background concentration




# Initialize the initial velocities to zero.
U = np.zeros((Ny,Nx))                           # x-Eulerian grid velocity
V = np.zeros((Ny,Nx))                           # y-Eulerian grid velocity
U.shape
mVelocity = np.zeros((mass_info.shape[0],2))  # mass-Pt velocity 

if arb_ext_force_Yes:
    print('  -Artificial External Forcing Onto Fluid Grid\n')
    firstExtForce = 1       # initialize external forcing
    indsExtForce = 0        # initialize for external forcing computation

# ACTUAL TIME-STEPPING IBM SCHEME! 
#(flags for storing structure connects for printing and printing to .vtk)
cter = 0 
ctsave = 0 
firstPrint = 1 
loc = 1 
diffy = 1

# CREATE VIZ_IB2D FOLDER, HIER_IB2D_DATA FOLDER FOR STORING .VTK DATA
try:
    os.mkdir('viz_IB2d')
except FileExistsError:
    #File already exists
    pass
if Output_Params[16] == 1:
    try:
        os.mkdir('hier_IB2d_data')
    except FileExistsError:
        #File already exists
        pass
os.chdir('viz_IB2d')

# PRINT BRINKMAN PERMEABILITY TO MATRIX (*if BRINKMAN TERM*) %
if brinkman_Yes:
    os.chdir('viz_IB2d')
    strNUM = give_String_Number_For_VTK(ctsave)
    brinkName = 'Brink.' + strNUM + '.vtk'
    savevtk_scalar(Brink.T, brinkName, 'Brinkman',dx,dy)
    #clear brinkName strNUM
    os.chdir('..')

#Save grid_Info and model_Info in human readable format
#with open('_grid_Info.txt','w') as fobj:
#    for key in sorted(grid_Info):
#        fobj.write('{0} = {1}\n'.format(key,grid_Info[key]))
#with open('_model_Info.txt','w') as fobj:
#    for key in sorted(model_Info):
#        fobj.write('{0} = {1}\n'.format(key,model_Info[key]))
#vizID = open('dumps.visit','w')
#vizID.write('!NBLOCKS 6\n')
#os.chdir('..')

#Initialize Vorticity, uMagnitude, and Pressure for initial colormap
#Print initializations to .vtk
vort = np.zeros((Nx,Ny)) 
uMag = np.array(vort) 
p = np.array(vort)
lagPts = np.zeros((xLag.size,3))
lagPts[:,0] = xLag 
lagPts[:,1] = yLag
connectsMat,spacing = give_Me_Lag_Pt_Connects(ds,xLag,yLag,Nx,springs_Yes,springs_info)
Fxh = np.array(vort) 
Fyh =np.array(vort) 
F_Lag = np.zeros((xLag.size,2)) 
print_vtk_files(Output_Params,ctsave,vort,uMag,p,U,V,Lx,Ly,Nx,Ny,lagPts,springs_Yes,\
connectsMat,tracers,concentration_Yes,C,Fxh,Fyh,F_Lag)
print('\n |****** Begin IMMERSED BOUNDARY SIMULATION! ******| \n\n')
print('Current Time(s): {0}\n'.format(current_time))
ctsave += 1

