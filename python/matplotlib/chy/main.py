#!/usr/bin/env python
# coding: utf-8

# In[46]:


import numpy as np
import matplotlib.pyplot as plt


# In[125]:


f1 = open("./data/002", "r")
lines = f1.readlines()
nl = len(lines)
sol = np.zeros([nl-2, 5])


# In[126]:


plt.close('all')
plt.figure() # 循环外


# In[130]:


for i in range(2, 11):
    tmp = "00" + str(i)
    idx = tmp[len(tmp)-3:len(tmp)]
    strname = "./data/" + idx
    f = open(strname, "r")
    lines = f.readlines()
    nl = len(lines)
    
    sol = np.zeros([nl-2, 5])
    for j in range(nl-2):
        tmp = lines[j+2].split()
        for k in range(5):
            sol[j, k] = tmp[k]
            
    plt.plot(sol[:, 2], sol[:, 1])
    plt.show()
    
    with open('data' + str(i) + '.npy', 'wb') as ff:
        np.save(ff, sol)


# In[ ]:




