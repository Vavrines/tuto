from julia.api import Julia
jl = Julia(compiled_modules=False)
from julia import KitBase as kt

from julia import Main

# python
import numpy as np

u = np.linspace(-5, 5, 24)
weights = np.ones(24) * 10 / 23
prim = np.array([1., 0., 1.])
M = kt.maxwellian(u, prim)
w = kt.prim_conserve(prim, 3)

kt.discrete_moments(M, u, weights, 0)
kt.moments_conserve(M, u, weights)

import copy as cp

M1 = cp.deepcopy(M)
M1.fill(0)

foo = jl.eval('KitBase.maxwellian!')
foo(M1, u, prim)

foo1 = jl.eval('KitBase.maxwellian')
foo1(u, prim)

kt.maxwellian_b(M1, u, prim)

f_b = Main.eval(
"""
begin
function f_b(dy,y)
    dy[1] = y[1]
    dy[2] = y[2]
    dy
end
end
"""
)

dy = np.array([0,0])
y = np.array([1, 3])

f_b(dy, y)
# this functionality is not supported in pyjulia yet
# https://github.com/JuliaPy/pyjulia/issues/441
# Here is the end of the work