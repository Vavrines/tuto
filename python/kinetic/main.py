# julia
from julia.api import Julia
jl = Julia(compiled_modules=False)
from julia import Kinetic

# python
import numpy as np

u = np.linspace(-5, 5, 24)
weights = np.ones(24) * 10 / 23
prim = np.array([1, 0, 1])
M = Kinetic.maxwellian(u, prim)
w = Kinetic.prim_conserve(prim, 3)

Kinetic.discrete_moments(M, u, weights, 0)
Kinetic.moments_conserve(M, u, weights)
