# %%
import numpy as np
from matplotlib import pyplot as plt

from julia.api import Julia
jl = Julia(compiled_modules=False)
from julia import KitBase as kt

# %%
nx = 40
dx = 1 / nx
nt = 25
dt = 0.01
c = 1

x = np.linspace(dx, 1-dx, nx)
u = np.ones(nx)
f = np.ones(nx+1)

for i in range(nx):
    u[i] = np.sin(2.0 * np.pi * x[i])

# %%
for n in range(nt):
    # flux
    for i in range(1, nx):
        f[i] = kt.flux_gks(u[i-1], u[i], 1e-6, dt, dx/2, dx/2, c)
    f[0] = kt.flux_gks(u[nx-1], u[0], 1e-6, dt, dx/2, dx/2, c)
    f[nx] = kt.flux_gks(u[nx-1], u[0], 1e-6, dt, dx/2, dx/2, c)   
    
    # update
    for i in range(0, nx):
        u[i] = u[i] + (f[i] - f[i+1]) / dx

# %%
plt.plot(x, u)
# %%
