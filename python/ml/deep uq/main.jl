using NPZ
using PyCall

np = pyimport("numpy")
pydata = np.load("./examples/spde/data/rbf_lx_002_ly_002_v_10.npz")
jldata = npzread("./examples/spde/data/rbf_lx_002_ly_002_v_10.npz")
cell = npzread("./examples/spde/data/cellcenters.npy")

"""
1000 samples, 32 × 32 domain
a ~ randn distribution --> should be log-normal as physical constraint

"""

jldata["inputs"]
jldata["outputs"]
