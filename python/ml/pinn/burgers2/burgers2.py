# https://zhuanlan.zhihu.com/p/636507529

# %%
import torch
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
from pyDOE import lhs

# %%
if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")


# %%
def weights_init(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight.data)
        torch.nn.init.zeros_(m.bias.data)


# %%
class DNN(nn.Module):
    def __init__(
        self, in_dim, hidden_dim, out_dim, depth: int, min, max, activation=nn.Tanh()
    ) -> None:
        super().__init__()
        self.in_layer = nn.Linear(in_dim, hidden_dim)
        self.hidden_layer = nn.Linear(hidden_dim, hidden_dim)
        self.layers = nn.ModuleList()
        for _ in range(depth):
            self.layers.append(self.hidden_layer)
        self.out_layer = nn.Linear(hidden_dim, out_dim)
        self.activation = activation
        self.min = min
        self.max = max

    def forward(self, x):
        x = (x - self.min) / (self.max - self.min)
        x = self.activation((self.in_layer(x)))
        for layer in self.layers:
            x = self.activation((layer(x)))
        x = self.activation((self.out_layer(x)))
        return x


# %%
class PINN:
    def __init__(self) -> None:
        self.iter = 0
        self.loss_dict = {"iters": [], "loss": []}
        Nf = 10000
        Nb = 100

        xt_domain = torch.tensor([[-1, 0], [1, 1]])
        min = xt_domain[0].to(device)
        max = xt_domain[1].to(device)
        self.xt_in = (xt_domain[1] - xt_domain[0]) * torch.tensor(
            lhs(2, Nf), dtype=torch.float
        ) + xt_domain[0]

        x_init = np.random.uniform(-1, 1, Nb)
        t_init = np.zeros_like(x_init)
        xt_init = np.stack([x_init, t_init], axis=1)

        t_bc1 = np.random.uniform(0, 1, Nb)
        x_bc1 = np.ones_like(t_bc1)
        xt_bc1 = np.stack([x_bc1, t_bc1], axis=1)

        t_bc2 = np.random.uniform(0, 1, Nb)
        x_bc2 = -1 * np.ones_like(t_bc2)
        xt_bc2 = np.stack([x_bc2, t_bc2], axis=1)

        self.xt_bc1 = torch.tensor(xt_bc1, dtype=torch.float)
        self.xt_bc2 = torch.tensor(xt_bc2, dtype=torch.float)
        self.xt_init = torch.tensor(xt_init, dtype=torch.float)

        self.xt_in = self.xt_in.to(device)
        self.xt_in.requires_grad_(True)
        self.xt_bc1 = self.xt_bc1.to(device)
        self.xt_bc2 = self.xt_bc2.to(device)
        self.xt_init = self.xt_init.to(device)

        self.net = DNN(2, 20, 1, depth=5, min=min, max=max).to(device)
        self.net.apply(weights_init)
        self.lossfn = torch.nn.MSELoss().to(device)

    def loss_compute(self):
        self.iter += 1

        u = self.net(self.xt_in)
        grads = torch.autograd.grad(u.sum(), self.xt_in, create_graph=True)[0]
        u_t = grads[:, 1]
        u_x = grads[:, 0]
        u_xx = torch.autograd.grad(u_x.sum(), self.xt_in, create_graph=True)[0][:, 0]
        res = u_t + u.squeeze() * u_x - 0.01 / torch.pi * u_xx
        loss_pde = self.lossfn(res, torch.zeros_like(res))

        u_init_exact = -torch.sin(torch.pi * self.xt_init[:, 0])
        u_init_pred = self.net(self.xt_init)
        loss_ic = self.lossfn(u_init_exact, u_init_pred.squeeze())

        u_bc1 = self.net(self.xt_bc1)
        u_bc2 = self.net(self.xt_bc2)
        u_bc = torch.cat([u_bc1, u_bc2, u_bc1 - u_bc2], dim=1)
        loss_bc = self.lossfn(u_bc, torch.zeros_like(u_bc))

        loss = +loss_pde + loss_ic + loss_bc
        self.loss_dict["iters"].append(self.iter)
        self.loss_dict["loss"].append(loss.item())
        return loss


if __name__ == "__main__":
    # %%
    pinn = PINN()

    adam = torch.optim.Adam(pinn.net.parameters(), lr=0.01)
    lbfgs = torch.optim.LBFGS(
        pinn.net.parameters(),
        lr=1.0,
        max_iter=50000,
        max_eval=50000,
        history_size=50,
        tolerance_grad=1e-5,
        tolerance_change=1.0 * np.finfo(float).eps,
        line_search_fn="strong_wolfe",  # better numerical stability
    )

    def closure():
        lbfgs.zero_grad()
        loss = pinn.loss_compute()
        loss.backward()
        print(f"\riter : {pinn.iter}     loss : {loss.item():.3e} ", end="")
        if pinn.iter % 300 == 0:
            print("")
        return loss

    # %%
    lbfgs.step(closure)
    # %%
    torch.save(pinn.net.state_dict(), "burgers.pt")
