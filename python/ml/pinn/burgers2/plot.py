# %%
import torch
from torch import nn
from burgers2 import PINN
import numpy as np
import matplotlib.pyplot as plt

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
# %%
pinn = PINN()
pinn.net.load_state_dict(torch.load('burgers.pt'))
x = np.linspace(-1, 1, 101)
t = np.linspace(0, 1, 51)
xx, tt = np.meshgrid(x, t, indexing="ij")
xt = torch.tensor(np.stack([xx.flatten(), tt.flatten()], axis=1), dtype=torch.float).to(device)
u_pred = pinn.net(xt)
# %%
fig = plt.figure(figsize=(12, 6))
ax = plt.subplot(111)
ax.set(xlabel='t', ylabel='x')
pred = ax.imshow(u_pred.cpu().detach().numpy().reshape(len(x), len(t)),
                 extent=[0, 1, -1, 1],
                 vmin=-1,
                 vmax=1,
                 aspect='auto')
plt.colorbar(pred, label="u(x,t)", ticks=[-1, -0.5, 0, 0.5, 1])
plt.show()
# %%
x = np.linspace(-1, 1, 101)
t1 = 0.25 * np.ones_like(x)
xt1 = torch.tensor(np.stack([x, t1], axis=1), dtype=torch.float).to(device)

t2 = 0.5 * np.ones_like(x)
xt2 = torch.tensor(np.stack([x, t2], axis=1), dtype=torch.float).to(device)

t3 = 0.75 * np.ones_like(x)
xt3 = torch.tensor(np.stack([x, t3], axis=1), dtype=torch.float).to(device)

fig = plt.figure(figsize=(14, 4))

ut1 = pinn.net(xt1)
ax1 = plt.subplot(131)
ax1.plot(x, ut1.cpu().detach().numpy(), c='b')
ax1.set(title='t=0.25s', xlabel='x', ylabel='u(x,t)')

ut2 = pinn.net(xt2)
ax2 = plt.subplot(132)
ax2.plot(x, ut2.cpu().detach().numpy(), c='b')
ax2.set(title='t=0.5s', xlabel='x')

ut3 = pinn.net(xt3)
ax3 = plt.subplot(133)
ax3.plot(x, ut3.cpu().detach().numpy(), c='b')
ax3.set(title='t=0.75s', xlabel='x')
plt.tight_layout()
plt.show()
# %%
