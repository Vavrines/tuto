# Simple PyTorch Implementation of Physics Informed Neural Network
# https://github.com/nanditadoloi/PINN/blob/main/solve_PDE_NN.ipynb

# %%
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# %%
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.hidden_layer1 = nn.Linear(2, 5)
        self.hidden_layer2 = nn.Linear(5, 5)
        self.hidden_layer3 = nn.Linear(5, 5)
        self.hidden_layer4 = nn.Linear(5, 5)
        self.hidden_layer5 = nn.Linear(5, 5)
        self.output_layer = nn.Linear(5, 1)

    def forward(self, x, t):
        inputs = torch.cat(
            [x, t], axis=1
        )
        layer1_out = torch.tanh(self.hidden_layer1(inputs))
        layer2_out = torch.tanh(self.hidden_layer2(layer1_out))
        layer3_out = torch.tanh(self.hidden_layer3(layer2_out))
        layer4_out = torch.tanh(self.hidden_layer4(layer3_out))
        layer5_out = torch.tanh(self.hidden_layer5(layer4_out))
        output = self.output_layer(
            layer5_out
        )
        return output

net = Net()

# %%
net = net.to(device)
mse_cost_function = torch.nn.MSELoss()  # Mean squared error
optimizer = torch.optim.Adam(net.parameters())
iterations = 10000

# %%
def f(x, t, net):
    u = net(
        x, t
    )
    u_x = torch.autograd.grad(u.sum(), x, create_graph=True)[0]
    u_t = torch.autograd.grad(u.sum(), t, create_graph=True)[0]
    pde = u_x - 2 * u_t - u
    return pde

# boundary: u(x,0)=6e^(-3x)
x_bc = np.random.uniform(low=0.0, high=2.0, size=(500, 1))
t_bc = np.zeros((500, 1))
u_bc = 6 * np.exp(-3 * x_bc)

# %%
for epoch in range(iterations):
    optimizer.zero_grad() # clean gradients

    # BC loss
    pt_x_bc = Variable(torch.from_numpy(x_bc).float(), requires_grad=False).to(device)
    pt_t_bc = Variable(torch.from_numpy(t_bc).float(), requires_grad=False).to(device)
    pt_u_bc = Variable(torch.from_numpy(u_bc).float(), requires_grad=False).to(device)

    net_bc_out = net(pt_x_bc, pt_t_bc)
    mse_u = mse_cost_function(net_bc_out, pt_u_bc)

    # PDE loss
    x_collocation = np.random.uniform(low=0.0, high=2.0, size=(500, 1))
    t_collocation = np.random.uniform(low=0.0, high=1.0, size=(500, 1))
    all_zeros = np.zeros((500, 1))

    pt_x_collocation = Variable(
        torch.from_numpy(x_collocation).float(), requires_grad=True
    ).to(device)
    pt_t_collocation = Variable(
        torch.from_numpy(t_collocation).float(), requires_grad=True
    ).to(device)
    pt_all_zeros = Variable(
        torch.from_numpy(all_zeros).float(), requires_grad=False
    ).to(device)

    f_out = f(pt_x_collocation, pt_t_collocation, net)
    mse_f = mse_cost_function(f_out, pt_all_zeros)

    # Combining losses
    loss = mse_u + mse_f

    loss.backward()
    optimizer.step()

    with torch.autograd.no_grad():
        print(epoch, "Traning Loss:", loss.data)

# %%
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

fig = plt.figure()
ax = fig.add_subplot(projection="3d")

x = np.arange(0, 2, 0.02)
t = np.arange(0, 1, 0.02)
ms_x, ms_t = np.meshgrid(x, t)
## Just because meshgrid is used, we need to do the following adjustment
x = np.ravel(ms_x).reshape(-1, 1)
t = np.ravel(ms_t).reshape(-1, 1)

pt_x = Variable(torch.from_numpy(x).float(), requires_grad=True).to(device)
pt_t = Variable(torch.from_numpy(t).float(), requires_grad=True).to(device)
pt_u = net(pt_x, pt_t)
u = pt_u.data.cpu().numpy()
ms_u = u.reshape(ms_x.shape)

surf = ax.plot_surface(
    ms_x, ms_t, ms_u, cmap=cm.coolwarm, linewidth=0, antialiased=False
)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter("%.02f"))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
