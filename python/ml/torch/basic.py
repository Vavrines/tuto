import torch
import torch.nn as nn
import torch.autograd as autograd

m = nn.Conv2d(16, 33, 3, 1) # in_channel, out_channel, kernel_size, stride

# non-square kernels and unequal stride and with padding
m = nn.Conv2d(16, 33, (3, 5), stride=(2, 1), padding=(4, 2))
# non-square kernels and unequal stride and with padding and dilation
m = nn.Conv2d(16, 33, (3, 5), stride=(2, 1), padding=(4, 2), dilation=(3, 1))

input = autograd.Variable(torch.randn(20, 16, 50, 100)) # batch, channel, x, y
output = m(input)
output.size()
