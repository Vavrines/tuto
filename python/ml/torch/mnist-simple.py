# %%
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
%matplotlib inline
# %%
print("Reading the data...")
data = pd.read_csv('../data/MNIST/train.csv', sep=",")
test_data = pd.read_csv('../data/MNIST/test.csv', sep=",")

print("Reshaping the data...")
dataFinal = data.drop('label', axis=1)
labels = data['label']

dataNp = dataFinal.values
labelsNp = labels.values
test_dataNp = test_data.values

print("Data is ready")
# %%
x = torch.FloatTensor(dataNp.tolist())
y = torch.LongTensor(labelsNp.tolist())
# %%
input_size = 784
output_size = 10
hidden_size = 200

epochs = 10
batch_size = 50
learning_rate = 0.00005
# %%
class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()
        self.l1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.l3 = nn.Linear(hidden_size, output_size)
        
    def forward(self, x):
        x = self.l1(x)
        x = self.relu(x)
        x = self.l3(x)
        return F.log_softmax(x, dim=1)
# %%
net = Network()
# %%
optimizer = optim.SGD(net.parameters(), lr=learning_rate, momentum=0.9)
loss_func = nn.CrossEntropyLoss()
# %%
for e in range(epochs):
    for i in range(0, x.shape[0], batch_size):
        x_mini = x[i:i + batch_size] 
        y_mini = y[i:i + batch_size] 
        
        x_var = Variable(x_mini)
        y_var = Variable(y_mini)
        
        optimizer.zero_grad()
        net_out = net(x_var)
        
        loss = loss_func(net_out, y_var)
        loss.backward()
        optimizer.step()
        
    print('Epoch: {} - Loss: {:.6f}'.format(e, loss.data.item()))
# %%
