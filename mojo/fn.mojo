from math import sqrt


fn patch(x: Int) -> Int:
    return x**2


fn patch(x: Float64) -> Float64:
    return sqrt(x)


fn rsqrt[dt: DType, width: Int](x: SIMD[dt, width]) -> SIMD[dt, width]:
    return 1 / sqrt(x)


fn para[T: AnyType](x: T) -> T:
    return x


fn main():
    let c = 3
    let x = 2.0
    print(patch(c))
    print(patch(x))
    print(rsqrt[DType.float64, 4](x))
    print(para(x))
