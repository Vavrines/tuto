"""
−∇(a(x,ω)∇u(x,ω)) = f(x)
x ∈ D = [0,1]², u(x,⋅) = 0 on ∂D
log(a(x,ω)) = z(x,ω)
"""

using GaussianRandomFields, SimpleMultigrid, MultilevelEstimators, Reporter, Plots

cd(@__DIR__)

pts = range(0, 1, 256) |> collect
nkl = 100
cov = CovarianceFunction(2, Matern(1 / 4, 3 / 4))
grf = GaussianRandomField(cov, KarhunenLoeve(nkl), pts, pts)

grfs = Vector{typeof(grf)}(undef, 7)
for i = 1:7
    n = 2^(i + 1)
    pts = 1/n:1/n:1-1/n
    grfs[i] = GaussianRandomField(cov, KarhunenLoeve(nkl), pts, pts)
end

function sample_lognormal(level::Level, ω::Vector, grf = grfs[level+one(level)])
    # solve on finest grid
    z = sample(grf, xi = ω)
    af = exp.(z)
    Af = elliptic2d(af)
    bf = fill(one(eltype(Af)), size(Af, 1))
    uf = Af \ bf
    Qf = uf[length(uf)÷2]

    # compute difference when not on coarsest grid
    dQ = Qf
    if level != Level(0)
        ac = view(af, 2:2:size(af, 1), 2:2:size(af, 2))
        Ac = elliptic2d(ac)
        bc = fill(one(eltype(Af)), size(Ac, 1))
        uc = Ac \ bc
        Qc = uc[length(uc)÷2]
        dQ -= Qc
    end
    dQ, Qf
end

distri = [Normal() for i = 1:nkl]
estimator = Estimator(ML(), MC(), sample_lognormal, distri, nb_of_warm_up_samples = 10)

h = run(estimator, 5e-3)
report(h)
