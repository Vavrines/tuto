#include "jlcxx/jlcxx.hpp"

// hello world function
std::string greet()
{
  return "hello, world!";
}

// simplest struct
struct MyStruct
{
  MyStruct() {}
};

// class with constructor, method and destructor
class World
{
public:
  World(const std::string &);
  void set(const std::string &);
  std::string greet(void);
  ~World();

protected:
  std::string msg;
};

World::World(const std::string &message = "hello world")
{
  this->msg = message;
}

void World::set(const std::string &message)
{
  this->msg = message;
}

std::string World::greet(void)
{
  return this->msg;
}

World::~World(void)
{
  std::cout << "Destroying World with message '" << this->msg << "'" << std::endl;
}

// inheritance
class Nation : public World // this needs to be public
{
public:
  Nation(const std::string &);
};

Nation::Nation(const std::string &s = "hello nation") : World(s)
{
}

// template (parametric) type

// arrays

namespace jlcxx
{
  // needed for upcasting
  template <>
  struct SuperType<Nation>
  {
    typedef World type;
  };
}

JLCXX_MODULE define_julia_module(jlcxx::Module &mod)
{
  mod.method("greet", &greet);
  mod.add_type<MyStruct>("MyStruct");
  mod.add_type<World>("World")
      .constructor<const std::string &>()
      .method("set", &World::set)
      .method("greet", &World::greet);
  mod.add_type<Nation>("Nation", jlcxx::julia_base_type<World>());
}
