# julia --project=. julia/cxxwrap/hello.jl

module CppHello
using CxxWrap

@wrapmodule(() -> joinpath(@__DIR__, "build/libhello"))
#@wrapmodule(() -> "/home/vavrines/Coding/tuto/julia/cxxwrap/build/libhello")

function __init__()
    @initcxx
end
end

# Call greet and show the result
@show CppHello.greet()

w = CppHello.World()
@show CppHello.greet(w) == "hello world"
CppHello.set(w, "hello new")
@show CppHello.greet(w) == "hello new"

v = CppHello.Nation()
@show CppHello.greet(v)
