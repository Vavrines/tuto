# julia --project=. julia/cxxwrap/hello.jl

module CppHello
using CxxWrap

@wrapmodule(() -> joinpath(@__DIR__, "build/libhello"))

function __init__()
    @initcxx
end
end

import Main.CppHello.TemplateType, Main.CppHello.P1, Main.CppHello.P2

p1 = TemplateType{P1, P2}()
p2 = TemplateType{P2, P1}()

@show CppHello.get_first(p1) == 1
@show CppHello.get_second(p2) == 1
