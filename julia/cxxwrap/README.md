# Tutorial

The CxxWrap package can be installed in the Julia package manager:
```julia
] add CxxWrap
```

The prefix path can obtained by running the following command:
```julia
using CxxWrap
CxxWrap.prefix_path()
```

After we get the path, we can use the CMake to build the library:
```bash
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=/path/to/prefix ..
make
```

On the Julia end, we can run the script with:
```bash
julia hello.jl
```