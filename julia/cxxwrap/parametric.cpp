#include <type_traits>
#include "jlcxx/jlcxx.hpp"

namespace parametric
{

    struct P1
    {
        typedef int val_type;
        static val_type value()
        {
            return 1;
        }
    };

    struct P2
    {
        typedef double val_type;
        static val_type value()
        {
            return 10.;
        }
    };

    template <typename A, typename B>
    struct TemplateType
    {
        typedef typename A::val_type first_val_type;
        typedef typename B::val_type second_val_type;

        first_val_type get_first()
        {
            return A::value();
        }

        second_val_type get_second()
        {
            return B::value();
        }
    };

    // Helper to wrap TemplateType instances. May also be a C++14 lambda, see README.md
    struct WrapTemplateType
    {
        template <typename TypeWrapperT>
        void operator()(TypeWrapperT &&wrapped)
        {
            typedef typename TypeWrapperT::type WrappedT;
            wrapped.method("get_first", &WrappedT::get_first);
            wrapped.method("get_second", &WrappedT::get_second);
        }
    };

} // namespace parametric

namespace jlcxx
{

    using namespace parametric;

    template <>
    struct IsMirroredType<P1> : std::false_type
    {
    };
    template <>
    struct IsMirroredType<P2> : std::false_type
    {
    };
    template <typename T1, typename T2>
    struct IsMirroredType<TemplateType<T1, T2>> : std::false_type
    {
    };

} // namespace jlcxx

JLCXX_MODULE define_julia_module(jlcxx::Module &types)
{
    using namespace jlcxx;
    using namespace parametric;

    types.add_type<P1>("P1");
    types.add_type<P2>("P2");

    types.add_type<Parametric<TypeVar<1>, TypeVar<2>>>("TemplateType")
        .apply<TemplateType<P1, P2>, TemplateType<P2, P1>>(WrapTemplateType());
}
