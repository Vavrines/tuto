using Kinetic, LoopVectorization

cd(@__DIR__)
ks, ctr, face, t = Kinetic.initialize("sod.txt")
dt = Kinetic.timestep(ks, ctr, t)

function flux_avx!(
    fw::X,
    fh::Y,
    fb::Y,
    hL::Z,
    bL::Z,
    hR::Z,
    bR::Z,
    u::A,
    ω::A,
    dt,
    shL = zeros(eltype(hL), axes(hL))::Z,
    sbL = zeros(eltype(bL), axes(bL))::Z,
    shR = zeros(eltype(hR), axes(hR))::Z,
    sbR = zeros(eltype(bR), axes(bR))::Z,
) where {
    X<:AbstractArray{<:AbstractFloat,1},
    Y<:AbstractArray{<:AbstractFloat,1},
    Z<:AbstractArray{<:AbstractFloat,1},
    A<:AbstractArray{<:AbstractFloat,1},
}

    #δ = similar(u)
    δ = heaviside.(u)

    h = similar(hL)
    b = similar(h)
    sh = similar(h)
    sb = similar(h)
    @avx for i in eachindex(u)
        #δ[i] = heaviside(u[i])

        h[i] = hL[i] * δ[i] + hR[i] * (1.0 - δ[i])
        b[i] = bL[i] * δ[i] + bR[i] * (1.0 - δ[i])

        sh[i] = shL[i] * δ[i] + shR[i] * (1.0 - δ[i])
        sb[i] = sbL[i] * δ[i] + sbR[i] * (1.0 - δ[i])
    end

    # calculate fluxes
    fw[1] = dt * sum(ω .* u .* h) - 0.5 * dt^2 * sum(ω .* u .^ 2 .* sh)
    fw[2] = dt * sum(ω .* u .^ 2 .* h) - 0.5 * dt^2 * sum(ω .* u .^ 3 .* sh)
    fw[3] =
        dt * 0.5 * (sum(ω .* u .^ 3 .* h) + sum(ω .* u .* b)) -
        0.5 * dt^2 * 0.5 * (sum(ω .* u .^ 4 .* sh) + sum(ω .* u .^ 2 .* sb))

    @avx for i in eachindex(u)
        fh[i] = dt * u[i] * h[i] - 0.5 * dt^2 * u[i]^2 * sh[i]
        fb[i] = dt * u[i] * b[i] - 0.5 * dt^2 * u[i]^2 * sb[i]
    end

    return nothing

end

@time for i = 2:ks.pSpace.nx
    flux_kfvs!(
        face[i].fw,
        face[i].fh,
        face[i].fb,
        ctr[i-1].h,
        ctr[i-1].b,
        ctr[i].h,
        ctr[i].b,
        ks.vSpace.u,
        ks.vSpace.weights,
        dt,
        ctr[i-1].sh,
        ctr[i-1].sb,
        ctr[i].sh,
        ctr[i].sb,
    )
end

@time for i = 2:ks.pSpace.nx
    flux_avx!(
        face[i].fw,
        face[i].fh,
        face[i].fb,
        ctr[i-1].h,
        ctr[i-1].b,
        ctr[i].h,
        ctr[i].b,
        ks.vSpace.u,
        ks.vSpace.weights,
        dt,
        ctr[i-1].sh,
        ctr[i-1].sb,
        ctr[i].sh,
        ctr[i].sb,
    )
end

"""
For current solver, @avx doesn't help at all.
I guess it may be due to we've already done a lot of normal operations which dominate the time consumption.
Need further investigations.

"""
