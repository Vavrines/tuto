using LoopVectorization

function A_mul_B!(𝐂, 𝐀, 𝐁)
    for m ∈ axes(𝐀, 1), n ∈ axes(𝐁, 2)
        𝐂ₘₙ = zero(eltype(𝐂))
        for k ∈ axes(𝐀, 2)
            𝐂ₘₙ += 𝐀[m, k] * 𝐁[k, n]
        end
        𝐂[m, n] = 𝐂ₘₙ
    end
end

function A_mul_B_avx!(𝐂, 𝐀, 𝐁)
    @avx for m ∈ axes(𝐀, 1), n ∈ axes(𝐁, 2)
        𝐂ₘₙ = zero(eltype(𝐂))
        for k ∈ axes(𝐀, 2)
            𝐂ₘₙ += 𝐀[m, k] * 𝐁[k, n]
        end
        𝐂[m, n] = 𝐂ₘₙ
    end
end

A = rand(2000, 2000)
B = rand(2000, 2000)
C = zeros(2000, 2000)

@time A_mul_B!(C, A, B)

@time A_mul_B_avx!(C, A, B)
