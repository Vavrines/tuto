using DiffEqFlux, Flux
using KitBase.FiniteMesh.DocStringExtensions
import Solaris

# ------------------------------------------------------------
# Define
# ------------------------------------------------------------

"""
$(TYPEDEF)
Affine layer of neural network
"""
struct Affine{T1<:AbstractArray,T2}
    w::T1
    b::T2
    σ::Function
end

function Affine(
    in::Integer,
    out::Integer,
    σ = identity::Function;
    fw = randn::Function,
    fb = zeros::Function,
    isBias = true::Bool,
    precision = Float32,
)
    if isBias
        return Affine(fw(precision, out, in), fb(precision, out), σ)
    else
        return Affine(fw(precision, out, in), Flux.Zeros(precision, out), σ)
    end
end

Flux.@functor Affine # works with Flux.params

(L::Affine)(x::AbstractVector) = L.w * x .+ L.b


"""
$(TYPEDEF)

Equivalent FastDense layer with controllable type
"""
struct FastAffine{I,F,F2} <: DiffEqFlux.FastLayer
    out::I
    in::I
    σ::F
    initial_params::F2
end

function FastAffine(
    in::Integer,
    out::Integer,
    σ = identity;
    fw = randn,
    fb = Flux.zeros,
    precision = Float32,
)
    initial_params() = vcat(vec(fw(precision, out, in)), fb(precision, out))
    return FastAffine(out, in, σ, initial_params)
end

# wx + b
(f::FastAffine)(x, p) =
    f.σ.(reshape(p[1:(f.out*f.in)], f.out, f.in) * x .+ p[(f.out*f.in+1):end])

DiffEqFlux.paramlength(f::FastAffine) = f.out * (f.in + 1)
DiffEqFlux.initial_params(f::FastAffine) = f.initial_params()

# ------------------------------------------------------------
# Test
# ------------------------------------------------------------

Affine(2, 1; isBias = true)
m = Affine(2, 1; isBias = false)
m(randn(Float32, 2))

Chain(4, 4, tanh)
Solaris.dense_layer(4, 4; isBias = true)
Solaris.dense_layer(4, 4; isBias = false)

faf = FastAffine(4, 4, tanh)
DiffEqFlux.paramlength(faf)
_p = initial_params(faf)
faf(randn(Float32, 4), _p)

nn = Chain(Dense(21, 21, tanh), Dense(21, 21))
sm = Flux.SkipConnection(nn, +)
show(sm)
sm(rand(21))
