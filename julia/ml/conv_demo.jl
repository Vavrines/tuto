using Flux

m1 = Conv((5, 5), 1 => 6, relu)
m2 = MaxPool((2, 2))
m3 = Conv((5, 5), 6 => 16, relu)
m4 = MaxPool((2, 2))

d0 = randn(Float32, 28, 28, 1, 100)
d1 = m1(d0)
d2 = m2(d1)
d3 = m3(d2)
d4 = m4(d3)

out_conv_size = (4, 4, 16)
prod(out_conv_size)

m = Chain(
    Conv((5, 5), 1 => 6, relu),
    MaxPool((2, 2)),
    Conv((5, 5), 6 => 16, relu),
    MaxPool((2, 2)),
    flatten,
    Dense(prod(out_conv_size), 10),
)

flatten(d4)

m(d0)
