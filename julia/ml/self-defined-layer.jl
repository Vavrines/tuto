using Flux

ann = Chain(Flux.Diagonal(5), Flux.Diagonal(5))

ann(randn(5))











W = rand(2, 5)
b = rand(2)

predict(x) = W * x .+ b

function loss(x, y)
    ŷ = predict(x)
    sum((y .- ŷ) .^ 2)
end

x, y = rand(5), rand(2) # Dummy data
loss(x, y) # ~ 3

using Flux

ps = params(W, b)
gs = gradient(() -> loss(x, y), ps)

using ForwardDiff, ReverseDiff

gs = ReverseDiff.gradient(() -> loss(x, y), params(W, b))



Wt = gs[W]
bt = gs[b]

W .-= 0.1 .* Wt
b .-= 0.1 .* bt
