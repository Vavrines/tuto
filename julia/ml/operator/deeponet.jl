using NeuralOperators, Flux, MAT, JLD2, Plots
using Flux: length, reshape, train!, throttle, @epochs

cd(@__DIR__)
device = cpu
#device = gpu
#@load "deeponet.jld2" model

#=
We would like to implement and train a DeepONet that infers the solution
u(x) of the burgers equation on a grid of 1024 points at time one based
on the initial condition a(x) = u(x,0)
=#

# Read the data from MAT file and store it in a dict
# key "a" is the IC
# key "u" is the desired solution at time 1
vars = matread("burgers_data_R10.mat") #|> device

# For trial purposes, we might want to train with a coarser resolution
# So we can sample only every n-th element
# if n = 8, we will have 8192/8 = 1024 points
subsample = 2^3

ntrain = 1000
ntest = 100

# create the x training array, according to our desired grid size
xtrain = vars["a"][1:ntrain, 1:subsample:end]' |> device

# create the x test array
xtest = vars["a"][end-ntest+1:end, 1:subsample:end]' |> device

# Create the y training array
ytrain = vars["u"][1:ntrain, 1:subsample:end] |> device
# Create the y test array
ytest = vars["u"][end-ntest+1:end, 1:subsample:end] |> device

# The data is missing grid data, so we create it
grid = collect(range(0, 1, length = 1024))' |> device

# Create the DeepONet:
# IC is given on grid of 1024 points, and we solve for a fixed time t in one
# spatial dimension x, making the branch input of size 1024 and trunk size 1
# We choose GeLU activation for both subnets
model = DeepONet((1024, 1024, 1024), (1, 1024, 1024), gelu, gelu) |> device
# This can be written alternatively as:
#model = begin
#    branch = Chain(Dense(1024, 1024), Dense(1024, 1024), Dense(1024, 1024))
#    trunk = Chain(Dense(1, 1024), Dense(1024, 1024))
#    DeepONet(branch, trunk)
#end

# We use the ADAM optimizer for training
learning_rate = 0.001
opt = ADAM(learning_rate)

# Specify the model parameters
parameters = Flux.params(model)

# The loss function
# We can't use the "vanilla" implementation of the mse here since we have
# two distinct inputs to our DeepONet, so we wrap them into a tuple
loss(xtrain, ytrain, sensor) = Flux.Losses.mse(model(xtrain, sensor), ytrain)

# Define a callback function that gives some output during training
evalcb() = @show(loss(xtest, ytest, grid))
# Print the callback only every 5 seconds
throttled_cb = throttle(evalcb, 5)

# Do the training loop
@epochs 500 train!(loss, parameters, [(xtrain, ytrain, grid)], opt, cb = evalcb)

#@save "deeponet.jld2" model

# Test
idx = 232
plot(grid[:], xtrain[:, idx])
plot!(grid[:], ytrain[idx, :])

Y = model(xtrain, grid)

plot!(grid[:], Y[idx, :])
