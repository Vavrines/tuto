using OrdinaryDiffEq, DiffEqSensitivity, Flux, DiffEqFlux, Optim
using ModelingToolkit, DataDrivenDiffEq
using LinearAlgebra, Plots

function lotka(du, u, p, t)
    α, β, γ, δ = p
    du[1] = α * u[1] - β * u[2] * u[1]
    du[2] = γ * u[1] * u[2] - δ * u[2]
end

tspan = (0.0f0, 3.0f0)
u0 = Float32[0.44249296, 4.6280594]
p_exact = Float32[1.3, 0.9, 0.8, 1.8]
prob = ODEProblem(lotka, u0, tspan, p_exact)
solution = solve(prob, Vern7(), abstol = 1e-12, reltol = 1e-12, saveat = 0.1)

# Ideal data
tsdata = Array(solution)
# Add noise to the data
noisy_data = tsdata + Float32(1e-5) * randn(eltype(tsdata), size(tsdata))

ann = FastChain(FastDense(2, 32, tanh), FastDense(32, 32, tanh), FastDense(32, 2))
p = initial_params(ann)

function dudt_(u, p, t)
    x, y = u
    z = ann(u, p)
    [p_exact[1] * x + z[1], -p_exact[4] * y + z[2]] # some model parts unknown
end
prob_nn = ODEProblem(dudt_, u0, tspan, p)

function loss(θ)
    pred =
        solve(
            prob_nn,
            Tsit5(),
            u0 = u0,
            p = θ,
            saveat = solution.t,
            abstol = 1e-6,
            reltol = 1e-6,
            sensealg = InterpolatingAdjoint(autojacvec = ReverseDiffVJP()),
        ) |> Array
    return sum(abs2, noisy_data .- pred), pred # use noisy data in loss
end

callback(θ, l, pred) = begin
    display(l)
    false
end

res = DiffEqFlux.sciml_train(loss, p, ADAM(0.01), cb = callback, maxiters = 100)
res = DiffEqFlux.sciml_train(
    loss,
    res.minimizer,
    BFGS(initial_stepnorm = 0.01),
    cb = callback,
    maxiters = 1000,
)

# Plot the data and the approximation
NNsolution = solve(prob_nn, Vern7(), u0 = u0, p = res.minimizer, saveat = solution.t)

# Trained on noisy data vs real solution
plot(solution)
scatter!(NNsolution)

# Collect the state trajectory and the derivatives
X = noisy_data
# Ideal derivatives
DX = Array(solution(solution.t, Val{1})) #- [p[1]*(X[1,:])';  -p[4]*(X[2,:])']

prob_nn2 = ODEProblem(dudt_, u0, tspan, res.minimizer)
_sol = solve(prob_nn2, Tsit5())
DX_ = Array(_sol(solution.t, Val{1}))

# The learned derivatives
plot(DX')
scatter!(DX_')

# The learned ann function
L = [-p_exact[2] * (X[1, :] .* X[2, :])'; p_exact[3] * (X[1, :] .* X[2, :])']
L̂ = ann(X, res.minimizer)
scatter(L')
plot!(L̂')

#--- SInDy algorithm ---#
# Create a Basis
@variables u[1:2]
# Lots of polynomials
polys = Operation[1]
for i ∈ 1:5
    push!(polys, u[1]^i)
    push!(polys, u[2]^i)
    for j ∈ i:5
        if i != j
            push!(polys, (u[1]^i) * (u[2]^j))
            push!(polys, u[2]^i * u[1]^i)
        end
    end
end

# And some other stuff
h = [cos.(u)...; sin.(u)...; polys...]
basis = Basis(h, u)

# Create an optimizer for the SINDY problem
opt = SR3()
# Create the thresholds which should be used in the search process
λ = exp10.(-6:0.1:2)
# Target function to choose the results from; x = L0 of coefficients and L2-Error of the model
f_target(x, w) = iszero(x[1]) ? Inf : norm(w .* x, 2)

# Test on original data and without further knowledge
Ψ = SInDy(X[:, :], DX[:, :], basis, λ, opt = opt, maxiter = 10000, f_target = f_target) # Fail
println(Ψ)
print_equations(Ψ)

# Test on ideal derivative data ( not available )
Ψ = SInDy(
    X[:, 5:end],
    L[:, 5:end],
    basis,
    λ,
    opt = opt,
    maxiter = 10000,
    f_target = f_target,
) # Succeed
println(Ψ)
print_equations(Ψ)

# Test on uode derivative data
Ψ = SInDy(
    X[:, 2:end],
    L̂[:, 2:end],
    basis,
    λ,
    opt = opt,
    maxiter = 10000,
    normalize = true,
    denoise = true,
    f_target = f_target,
) # Succeed
println(Ψ)
print_equations(Ψ)
p̂ = parameters(Ψ)

# Create function
unknown_sys = ODESystem(Ψ)
unknown_eq = ODEFunction(unknown_sys)
# Just the equations
b = Basis((u, p, t) -> unknown_eq(u, [1.0; 1.0], t), u)
# Retune for better parameters -> we could also use DiffEqFlux or other parameter estimation tools here.
Ψf = SInDy(
    noisy_data[:, 2:end],
    L̂[:, 2:end],
    b,
    opt = SR3(0.01),
    maxiter = 100,
    convergence_error = 1e-18,
) # Succeed
println(Ψf)
p̂ = parameters(Ψf)

# Create function
unknown_sys = ODESystem(Ψf)
unknown_eq = ODEFunction(unknown_sys)

# Build an ODE for the estimated system
function approx(du, u, p, t)
    # Add SInDy Term
    α, δ, β, γ = p
    z = unknown_eq(u, [β; γ], t)
    du[1] = α * u[1] + z[1]
    du[2] = -δ * u[2] + z[2]
end

# Create the approximated problem and solution
ps = [p_exact[[1, 4]]; p̂]
a_prob = ODEProblem(approx, u0, tspan, ps)
a_solution = solve(a_prob, Tsit5(), saveat = 0.1)

plot(solution)
plot!(a_solution)

# Look at long term prediction
t_long = (0.0, 50.0)
a_prob = ODEProblem(approx, u0, t_long, ps)
a_solution = solve(a_prob, Tsit5()) # Using higher tolerances here results in exit of julia
plot(a_solution)

prob_true2 = ODEProblem(lotka, u0, t_long, p_exact)
solution_long = solve(prob_true2, Tsit5(), saveat = a_solution.t)
plot!(solution_long)

p1 = plot(
    0.1:0.1:tspan[end],
    abs.(Array(solution)[:, 2:end] .- NNsolution[:, 2:end])' .+ eps(Float32),
    lw = 3,
    yaxis = :log,
    title = "Timeseries of UODE Error",
    color = [3 :orange],
    xlabel = "t",
    label = ["x(t)" "y(t)"],
    titlefont = "Helvetica",
    legendfont = "Helvetica",
    legend = :topright,
)

p2 = plot(
    X[1, :],
    X[2, :],
    L[2, :],
    lw = 3,
    title = "Neural Network Fit of U2(t)",
    color = 3,
    label = "Neural Network",
    xaxis = "x",
    yaxis = "y",
    titlefont = "Helvetica",
    legendfont = "Helvetica",
    legend = :bottomright,
)
plot!(X[1, :], X[2, :], L̂[2, :], lw = 3, label = "True Missing Term", color = :orange)

c1 = 3 # RGBA(174/255,192/255,201/255,1) # Maroon
c2 = :orange # RGBA(132/255,159/255,173/255,1) # Red
c3 = :blue # RGBA(255/255,90/255,0,1) # Orange
c4 = :purple # RGBA(153/255,50/255,204/255,1) # Purple

p3 = scatter(
    solution,
    color = [c1 c2],
    label = ["x data" "y data"],
    title = "Extrapolated Fit From Short Training Data",
    titlefont = "Helvetica",
    legendfont = "Helvetica",
    markersize = 5,
)

plot!(
    p3,
    solution_long,
    color = [c1 c2],
    linestyle = :dot,
    lw = 5,
    label = ["True x(t)" "True y(t)"],
)
plot!(p3, a_solution, color = [c3 c4], lw = 1, label = ["Estimated x(t)" "Estimated y(t)"])
plot!(
    p3,
    [2.99, 3.01],
    [0.0, maximum(hcat(Array(solution), Array(a_solution)))],
    lw = 2,
    color = :black,
)
