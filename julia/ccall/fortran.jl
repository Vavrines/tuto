using Kinetic

h = zeros(24)
b = zeros(24)
u = ones(24)
ω = ones(24)
prim = [1.0, 0.0, 1.0]

cd(@__DIR__)
ccall(
    (:__kinetic_MOD_heat_flux, "kinetic.so"),
    Float64,
    (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
    h,
    b,
    prim,
    u,
    ω,
)

ccall(
    (:heat_flux_, "test.so"),
    Float64,
    (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
    h,
    b,
    prim,
    u,
    ω,
)
