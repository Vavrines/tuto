
! kinetic.f90
!-------------------------------------------------------------------

module Kinetic

implicit none

real(kind=8), parameter :: PI = 3.141592654d0

integer, parameter :: MNUM = 6 !number of normal velocity moments
integer, parameter :: MTUM = 6 !number of tangential velocity moments

contains


function heat_flux(h, b, prim, uspace, weight)
    
    real(kind=8), dimension(:), intent(in) :: h, b
    real(kind=8), intent(in) :: prim(3)
    real(kind=8), dimension(:), intent(in) :: uspace, weight
    real(kind=8) :: heat_flux

    heat_flux = 0.5d0 * (sum(weight * (uspace - prim(2)) * (uspace - prim(2))**2 * h) + &
            sum(weight * (uspace - prim(2)) * b))

end function heat_flux

function heat_flux_julia(h, b, prim, uspace, weight, n)
    
    integer, intent(in) :: n
    real(kind=8), intent(in) :: h(n), b(n)
    real(kind=8), intent(in) :: prim(3)
    real(kind=8), intent(in) :: uspace(n), weight(n)
    real(kind=8) :: heat_flux_julia

    heat_flux_julia = heat_flux(h, b, prim, uspace, weight)

end function heat_flux_julia

end module Kinetic