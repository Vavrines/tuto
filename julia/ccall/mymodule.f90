module mymodule

use iso_c_binding

implicit none

integer, parameter :: MNUM = 6

contains

    subroutine test(str1, str2)
        character(len=*), intent(inout)  :: str1, str2

        str1 = "modified"
        write(*,*) str1, str2
        WRITE(*,*) 'x = ', str1
    end

    function add(x, y)
        REAL*8 add
        REAL*8 x, y
        WRITE(*,*) 'x = ', x
        WRITE(*,*) 'y = ', y
        add = x + y
    end

    subroutine toyflux(wL, hL, unum)
        real(kind=8), dimension(:), intent(in) :: wL
        real(kind=8), dimension(:), intent(in) :: hL
        integer, intent(in) :: unum
        
        real(kind=8), allocatable, dimension(:) :: h
        allocate(h(unum))
        
        h(:) = 0.d0
        write(*,*) h(1)
    end
    
end module mymodule

