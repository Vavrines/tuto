! fcode.f90
      
FUNCTION add(x, y)
    real(kind=8)::add
    real(kind=8)::x, y

    add = x + y
END

subroutine modify_string(str1, str2)
    character(len=*), intent(inout)::str1, str2
    str1 = str2
end

subroutine toy_flux(wL, hL, unum)
    real(kind=8), dimension(:), intent(in) :: wL
    real(kind=8), dimension(:), intent(in) :: hL
    integer, intent(in) :: unum

    real(kind=8), allocatable, dimension(:) :: h
    allocate(h(unum))

    h(:) = 0.d0
    write(*,*) h(1)
end

function heat_flux(h, b, prim, uspace, weight)
    
    real(kind=8), dimension(:), intent(in) :: h, b
    real(kind=8), intent(in) :: prim(3)
    real(kind=8), dimension(:), intent(in) :: uspace, weight
    real(kind=8) :: heat_flux

    !heat_flux = 0.5d0 * (sum(weight * (uspace - prim(2)) * (uspace - prim(2))**2 * h) + &
    !            sum(weight * (uspace - prim(2)) * b))
    
    heat_flux = sum(h)
    
end function heat_flux

subroutine foo3(x,y)
    real*8, intent(in) :: x(:)
    real*8, intent(out) :: y(:)
    y = 2*x
    write(*,*) y
end subroutine foo3

function foo4(h)
    real(kind=8) :: foo4
    real(kind=8), intent(in) :: h(:)

    !foo4 = sum(h)
    foo4 = h(1)+h(2)
    
end