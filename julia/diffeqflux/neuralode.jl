using OrdinaryDiffEq, Lux, DiffEqFlux, ComponentArrays, Random, Optimization
using CairoMakie, NipponColors

cd(@__DIR__)
dc = dict_color()
pc = plot_color()

using Lux, DiffEqFlux, DifferentialEquations, Optimization, OptimizationOptimJL, Random, Plots

rng = Random.default_rng()
u0 = Float32[2.0; 0.0]
datasize = 100
tspan = (0.0f0, 1.5f0)
tsteps = range(tspan[1], tspan[2], length = datasize)

function trueODEfunc(du, u, p, t)
	true_A = [-0.1 2.0; -2.0 -0.1]
	du .= ((u .^ 3)'true_A)'
end

prob_trueode = ODEProblem(trueODEfunc, u0, tspan)
ode_data = Array(solve(prob_trueode, Tsit5(), saveat = tsteps))

dudt2 = Lux.Chain(x -> x .^ 3,
	Lux.Dense(2, 50, tanh),
	Lux.Dense(50, 2))
p, st = Lux.setup(rng, dudt2)
prob_neuralode = NeuralODE(dudt2, tspan, Tsit5(), saveat = tsteps)

function predict_neuralode(p)
	Array(prob_neuralode(u0, p, st)[1])
end

function loss_neuralode(p)
	pred = predict_neuralode(p)
	loss = sum(abs2, ode_data .- pred)
	return loss, pred
end

# Do not plot by default for the documentation
# Users should change doplot=true to see the plots callbacks
callback = function (p, l, pred; doplot = false)
	println(l)
	return false
end

pinit = ComponentArray(p)

# use Optimization.jl to solve the problem
adtype = Optimization.AutoZygote()

optf = Optimization.OptimizationFunction((x, p) -> loss_neuralode(x), adtype)
optprob = Optimization.OptimizationProblem(optf, pinit)

result_neuralode = Optimization.solve(optprob,
	ADAM(0.05),
	callback = callback,
	maxiters = 300)

optprob2 = remake(optprob, u0 = result_neuralode.u)

result_neuralode2 = Optimization.solve(optprob2,
	Optim.BFGS(initial_stepnorm = 0.01),
	callback = callback,
	allow_f_increases = false)

pred = predict_neuralode(result_neuralode2.u)

begin
    fig = Figure()
    ax = Axis(fig[1, 1], xlabel = "t", ylabel = "u", title = "")
    lines!(tsteps, ode_data[1, :]; color = dc["ro"], label = "true")
    lines!(tsteps, ode_data[2, :]; color = dc["ro"])
    scatter!(tsteps, pred[1, :]; color = (dc["ohni"], 0.7), label = "prediction")
    scatter!(tsteps, pred[2, :]; color = (dc["ohni"], 0.7))
    axislegend()
    fig
end
save("ode_in.pdf", fig)

tspan2 = (0.0f0, 5.0f0)
tsteps2 = range(tspan2[1], tspan2[2], length = 100)
prob2 = ODEProblem(trueODEfunc, u0, tspan2)
ode_data2 = Array(solve(prob2, Tsit5(), saveat = tsteps2))

prob_neuralode2 = NeuralODE(dudt2, tspan2, Tsit5(), saveat = tsteps2)
pred2 = Array(prob_neuralode2(u0, result_neuralode2.u, st)[1])

begin
    fig = Figure()
    ax = Axis(fig[1, 1], xlabel = "t", ylabel = "u", title = "")
    lines!(tsteps2, ode_data2[1, :]; color = dc["ro"], label = "true")
    lines!(tsteps2, ode_data2[2, :]; color = dc["ro"])
    scatter!(tsteps2, pred2[1, :]; color = (dc["ohni"], 0.7), label = "prediction")
    scatter!(tsteps2, pred2[2, :]; color = (dc["ohni"], 0.7))
    axislegend()
    fig
end
save("ode_out.pdf", fig)
