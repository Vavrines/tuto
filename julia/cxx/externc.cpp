// Using extern "C" to create callables for julia
// compilation: g++ -fPIC -shared -o externc.so externc.cpp

extern "C"
{
    int add(int, int); // declare things
}

// definitions
int add(int m, int n)
{
    return m + n;
}
