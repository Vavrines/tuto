import numpy

from numpy import linalg

from numpy import random, zeros, array
from numpy import sum as npsum
from time import time

def mlmc_fn(l, N, problems, coupled_problem=False, sampler=None, N1 = 1):
    """
    Inputs:
        l: level
        N: number of paths
        problems: list of problems
            problems[l-1]: application-specific coarse problem (for l>0)
            problems[l]: application-specific fine problem 
            Problems must have an evaluate method such that
            problems[l].evaluate(sample) returns output P_l.
            Optionally, user-defined problems.cost
        coupled_problem: if True,
             problems[l].evaluate(sample) returns both P_l and P_{l-1}.
        sampler: sampling function, by default standard Normal.
            input: N, l
            output: (samplef, samplec). The fine and coarse samples.
         N1: number of paths to generate concurrently.

    Outputs:
        (sums, cost) where sums is an array of outputs:
        sums[0] = sum(Pf-Pc)
        sums[1] = sum((Pf-Pc)**2)
        sums[2] = sum((Pf-Pc)**3)
        sums[3] = sum((Pf-Pc)**4)
        sums[4] = sum(Pf)
        sums[5] = sum(Pf**2)
        cost = user-defined computational cost. By default, time
    """

    if sampler is None:
        def sampler(N, l):
            sample = random.randn(N)
            return (sample, sample)

    sums = zeros(6)
    cpu_cost = 0.0
    problemf = problems[l]
    if l>0:
        problemc = problems[l-1]

    for i in range(1, N+1, N1):
        N2 = min(N1, N - i + 1)

        samplef, samplec = sampler(N2,l)

        start = time()
        if coupled_problem:
            Pf, Pc = problems[l].evaluate(samplef) 
        else:
            Pf = problemf.evaluate(samplef)
            if l == 0:
                Pc = 0.
            else:
                Pc = problemc.evaluate(samplec)
            
        end = time()
        cpu_cost += end - start # cost defined as total computational time
        sums += array([npsum(Pf - Pc),
                       npsum((Pf - Pc)**2),
                       npsum((Pf - Pc)**3),
                       npsum((Pf - Pc)**4),
                       npsum(Pf),
                       npsum(Pf**2)])


    problem_cost_defined = hasattr(problemf, 'cost')
    problem_cost_defined = problem_cost_defined and problemf.cost is not None

    if problem_cost_defined:
        cost = N*problemf.cost
        if l>0:
            cost += N*problemc.cost # user-defined problem-specific cost
    else:
        cost = cpu_cost

    return (sums, cost)



class WeakConvergenceFailure(Exception):
    pass

def mlmc(Lmin, Lmax, N0, eps, mlmc_fn, alpha_0, beta_0, gamma_0, *args, **kwargs):
    """
    Multilevel Monte Carlo estimation.

    (P, Nl, Cl) = mlmc(...)

    Inputs:
      N0:   initial number of samples    >  0
      eps:  desired accuracy (rms error) >  0
      Lmin: minimum level of refinement  >= 2
      Lmax: maximum level of refinement  >= Lmin

      mlmc_fn: the user low-level routine for level l estimator. Its interface is

        (sums, cost) = mlmc_fn(l, N, *args, **kwargs)

        Inputs:  l: level
                 N: number of paths
                 *args, **kwargs: optional additional user variables

        Outputs: sums[0]: sum(Y)
                 sums[1]: sum(Y**2)
                    where Y are iid samples with expected value
                        E[P_0]            on level 0
                        E[P_l - P_{l-1}]  on level l > 0
                 cost: cost of N samples

      alpha ->  weak error is  O(2^{-alpha*l})
      beta  ->  variance is    O(2^{-beta*l})
      gamma ->  sample cost is O(2^{ gamma*l})

      If alpha, beta are not positive then they will be estimated.

      *args, **kwargs = optional additional user variables to be passed to mlmc_fn

    Outputs:
      P:  value
      Nl: number of samples at each level
      Cl: cost of samples at each level
    """

    # Check arguments

    if Lmin < 2:
        raise ValueError("Need Lmin >= 2")
    if Lmax < Lmin:
        raise ValueError("Need Lmax >= Lmin")
    if N0 <= 0 or eps <= 0:
        raise ValueError("Need N0 > 0, eps > 0")

    # Initialisation

    alpha = max(0, alpha_0)
    beta  = max(0, beta_0)
    gamma = max(0, gamma_0)

    theta = 0.25

    L = Lmin

    Nl   = numpy.zeros(L+1)
    suml = numpy.zeros((2, L+1))
    costl = numpy.zeros(L+1)
    dNl  = N0*numpy.ones(L+1)

    while sum(dNl) > 0:

        # update sample sums

        for l in range(0, L+1):
            if dNl[l] > 0:
                (sums, cost) = mlmc_fn(l, int(dNl[l]), *args, **kwargs)
                Nl[l]        = Nl[l] + dNl[l]
                suml[0, l]   = suml[0, l] + sums[0]
                suml[1, l]   = suml[1, l] + sums[1]
                costl[l]     = costl[l] + cost

        # compute absolute average, variance and cost

        ml = numpy.abs(       suml[0, :]/Nl)
        Vl = numpy.maximum(0, suml[1, :]/Nl - ml**2)
        Cl = costl/Nl

        # fix to cope with possible zero values for ml and Vl
        # (can happen in some applications when there are few samples)

        for l in range(3, L+2):
            ml[l-1] = max(ml[l-1], 0.5*ml[l-2]/2**alpha)
            Vl[l-1] = max(Vl[l-1], 0.5*Vl[l-2]/2**beta)

        # use linear regression to estimate alpha, beta, gamma if not given
        if alpha_0 <= 0:
            A = numpy.ones((L, 2)); A[:, 0] = range(1, L+1)
            x = numpy.linalg.lstsq(A, numpy.log2(ml[1:]))[0]
            alpha = max(0.5, -x[0])

        if beta_0 <= 0:
            A = numpy.ones((L, 2)); A[:, 0] = range(1, L+1)
            x = numpy.linalg.lstsq(A, numpy.log2(Vl[1:]))[0]
            beta = max(0.5, -x[0])

        if gamma_0 <= 0:
            A = numpy.ones((L, 2)); A[:, 0] = range(1, L+1)
            x = numpy.linalg.lstsq(A, numpy.log2(Cl[1:]))[0]
            gamma = max(0.5, x[0])

        # set optimal number of additional samples

        Ns = numpy.ceil( numpy.sqrt(Vl/Cl) * sum(numpy.sqrt(Vl*Cl)) / ((1-theta)*eps**2) )
        dNl = numpy.maximum(0, Ns-Nl)

        # if (almost) converged, estimate remaining error and decide
        # whether a new level is required

        if sum(dNl > 0.01*Nl) == 0:
            # 23/3/18 this change copes with cases with erratic ml values
            rang = list(range(min(3, L)))
            rem = ( numpy.amax(ml[[L-x for x in rang]] / 2.0**(numpy.array(rang)*alpha))
                    / (2.0**alpha - 1.0) )
            # rem = ml[L] / (2.0**alpha - 1.0)

            if rem > numpy.sqrt(theta)*eps:
                if L == Lmax:
                    raise WeakConvergenceFailure("Failed to achieve weak convergence")
                else:
                    L = L + 1
                    Vl = numpy.append(Vl, Vl[-1] / 2.0**beta)
                    Nl = numpy.append(Nl, 0.0)
                    suml = numpy.column_stack([suml, [0, 0]])
                    Cl = numpy.append(Cl, Cl[-1]*2**gamma)
                    costl = numpy.append(costl, 0.0)

                    Ns = numpy.ceil( numpy.sqrt(Vl/Cl) * sum(numpy.sqrt(Vl*Cl))
                            / ((1-theta)*eps**2) )
                    dNl = numpy.maximum(0, Ns-Nl)

    # finally, evaluate the multilevel estimator
    P = sum(suml[0,:]/Nl)

    return (P, Nl, Cl)
