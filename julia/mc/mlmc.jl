using PyCall, Conda

Conda.pip_interop(true)
Conda.pip("install", "mlmcpy")
Conda.pip("install", "uncertainpy")

using PyCall
pushfirst!(PyVector(pyimport("sys")."path"), "/home/vavrines/Coding/tuto/julia/mc/pymlmc")
pymlmc = pyimport("pymlmc")

py"""
import sys
sys.path.append("/home/vavrines/Coding/tuto/julia/mc/pymlmc")
from pymlmc import mlmc_test, mlmc_plot
"""
