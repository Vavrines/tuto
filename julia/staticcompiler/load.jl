using StaticCompiler

cd(@__DIR__)

f1 = load_function("f1")
f1(2)
f1(2.0) # error

f2 = load_function("f2")
f2(2.0, 2)

f3 = load_function("f3")
f3([2.0, 3.0])

f4 = load_function("prim_conserve")
f4([1.0, 0.0, 1.0], 5 / 3)
