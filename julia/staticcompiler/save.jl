using StaticCompiler
using KitBase

cd(@__DIR__)

f1(n) = begin
    n^2
end
compile(f1, Tuple{Int}, "f1")
compile(f1, (Int,), "f1")

f2(x, y) = begin
    x^2, y + 1
end
compile(f2, (Float64, Int), "f2")

f3(x) = begin
    s = zero(x)
    for i in eachindex(x)
        s[i] = x[i]^2
    end
    return s
end
#f3(x) = x .^ 2 # error
compile(f3, (Vector{Float64},), "f3")

compile(KB.prim_conserve, (Vector{Float64}, Float64), "prim_conserve")
