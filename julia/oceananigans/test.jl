using Oceananigans

N = Nx = Ny = Nz = 32#128
L = Lx = Ly = Lz = 2000
topology = (Periodic, Periodic, Bounded)

model = IncompressibleModel(
    architecture = CPU(),
    grid = RegularCartesianGrid(
        topology = topology,
        size = (Nx, Ny, Nz),
        extent = (Lx, Ly, Lz),
    ),
    closure = IsotropicDiffusivity(ν = 4e-2, κ = 4e-2),
)

# Set a temperature perturbation with a Gaussian profile located at the center.
# This will create a buoyant thermal bubble that will rise with time.
x₀, z₀ = Lx / 2, Lz / 2
T₀(x, y, z) = 20 + 0.01 * exp(-100 * ((x - x₀)^2 + (z - z₀)^2) / (Lx^2 + Lz^2))
set!(model, T = T₀)

simulation = Simulation(model, Δt = 10, stop_iteration = 1000)
run!(simulation)

struct test{T}
    a::T
end

model.velocities.u.data

model.grid
