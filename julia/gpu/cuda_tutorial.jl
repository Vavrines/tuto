using CUDA, Test

###
# let's start from the non-optimal codes
###

function add0!(y, x)
    for i = 1:length(y)
        @inbounds y[i] += x[i]
    end

    return nothing
end

function add1!(y, x)
    index = threadIdx().x # this example only requires linear indexing, so just use `x`
    stride = blockDim().x
    for i = index:stride:length(y)
        @inbounds y[i] += x[i]
    end

    return nothing
end

N = 256
x = CUDA.randn(N)
y0 = CUDA.zeros(N)
y1 = CUDA.zeros(N)

@cuda add0!(y0, x)
@cuda threads = 256 add1!(y1, x)
@test y0 == y1

function add2!(y, x)
    idx = threadIdx().x
    idy = threadIdx().y
    strx = blockDim().x
    stry = blockDim().y
    for j = idy:stry:size(y, 2)
        for i = idx:strx:size(y, 1)
            @inbounds y[i, j] += x[i, j]
        end
    end

    return nothing
end

y2 = CUDA.zeros(N, N)
x2 = CUDA.randn(N, N)

@cuda threads = 256 add2!(y2, x2)

###
# now we optimize the codes
###

function oadd1!(y, x)
    index = (blockIdx().x - 1) * blockDim().x + threadIdx().x
    stride = blockDim().x * gridDim().x
    for i = index:stride:length(y)
        @inbounds y[i] += x[i]
    end

    return nothing
end

yo = CUDA.zeros(N)
@cuda threads = 256 oadd1!(yo, x)
@test yo == y1

function oadd2!(y, x)
    idx = (blockIdx().x - 1) * blockDim().x + threadIdx().x
    idy = (blockIdx().y - 1) * blockDim().y + threadIdx().y
    strx = blockDim().x * gridDim().x
    stry = blockDim().y * gridDim().y
    Nx, Ny = size(y)

    for j = idy:stry:Ny
        for i = idx:strx:Nx
            @inbounds y[i, j] += x[i, j]
        end
    end

    return nothing
end

yo2 = CUDA.zeros(N, N)
@cuda threads = 256 blocks = 256 oadd2!(yo2, x2)
@test yo2 == y2

###
# division
###

function divide0!(y, a, b)
    for j = 1:size(y, 2)
        for i = 1:size(y, 1)
            @inbounds y[i, j] = a[i, j] / b[i]
        end
    end

    return nothing
end

function divide1!(y, a, b)
    idx = threadIdx().x
    idy = threadIdx().y
    strx = blockDim().x
    stry = blockDim().y
    for j = idy:stry:size(y, 2)
        for i = idx:strx:size(y, 1)
            @inbounds y[i, j] = a[i, j] / b[i]
        end
    end

    return nothing
end

function divide2!(y, a, b)
    idx = (blockIdx().x - 1) * blockDim().x + threadIdx().x
    idy = (blockIdx().y - 1) * blockDim().y + threadIdx().y
    strx = blockDim().x * gridDim().x
    stry = blockDim().y * gridDim().y
    Nx, Ny = size(y)

    for j = idy:stry:Ny
        for i = idx:strx:Nx
            @inbounds y[i, j] = a[i, j] / b[i]
        end
    end

    return nothing
end

function divide2_cart!(y, a, b)
    id = (blockIdx().x - 1) * blockDim().x + threadIdx().x
    stride = blockDim().x * gridDim().x

    Nx, Ny = size(y)

    cind = CartesianIndices((Nx, Ny))

    for k = id:stride:Nx*Ny
        i = cind[k][1]
        j = cind[k][2]
        @inbounds y[i, j] = a[i, j] / b[i]
    end

    return nothing
end

a = rand(32, 16) |> CuArray
b = rand(32) |> CuArray

y0 = zero(a)
y1 = zero(a)
y2 = zero(a)

@cuda divide0!(y0, a, b)
@cuda threads = 32 divide1!(y1, a, b)
@cuda threads = 32 blocks = 16 divide2!(y2, a, b)
@test y0 == y1 == y2
