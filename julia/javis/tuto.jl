using Javis

# applied on every frame
function ground(args...)
    background("white") # canvas background
    sethue("black") # pen color
end

# draw a circle
function object(p = O, color = "black")
    sethue(color)
    circle(p, 25, :fill)
    return p
end

# draw dotted points
function path!(points, pos, color)
    sethue(color)
    push!(points, pos)
    circle.(points, 2, :fill)
end

# center for the two objects
p1 = Point(100, 0)
p2 = Point(100, 80)
p3 = Point(0, 70)

# dotted points vectors
path1 = Point[]
path2 = Point[]
path3 = Point[]

# video struct (width, height)
myvideo = Video(500, 500)

javis(
    myvideo,
    [
        BackgroundAction(1:80, ground), # set background color and pen color
        Action(1:80, :ball1, (args...) -> object(p1, "brown"), Rotation(0.0, 2π)), # draw the red ball with rotation
        Action(
            1:80,
            :ball2,
            (args...) -> object(p2, "springgreen4"),
            Rotation(2π, 0.0, :ball1),
        ), # draw the blue ball with dynamic rotation
        Action(
            1:80,
            :ball3,
            (args...) -> object(p3, "darkorchid"),
            Rotation(2π, 0.0, :ball1),
        ), # draw the blue ball with dynamic rotation
        Action(1:80, (args...) -> path!(path1, pos(:ball1), "burlywood")), # draw tracks for red ball
        Action(1:80, (args...) -> path!(path2, pos(:ball2), "darkseagreen1")), # draw track for blue ball
        Action(1:80, (args...) -> path!(path3, pos(:ball3), "violet")), # draw track for blue ball
    ],
    pathname = "dancing_circles.gif", # path with output file name
)
