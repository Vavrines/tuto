using Random, Plots, ProgressMeter

# Set option flags and key parameters for the simulation
BC_FLAG = 1                   # Boundary condition flag (0: Periodic; 1: Dirichlet)
NONEQ_FLAG = 0                # Non-equilibrium flag (0: Thermodynamic equilibrium; 1: Temperature gradient)
if BC_FLAG == 0 && NONEQ_FLAG == 1
    @warn "ERROR: Periodic boundary conditions are only for thermodynamic equilibrium"
    NONEQ_FLAG = 0            # Reset the flag to equilibrium
end
PERTURB_FLAG = 1              # Start from perturbed initial condition (0: No; 1: Yes)
STOCH_FLAG = 1                # Thermal fluctuations? (0: No, deterministic; 1: Yes, stochastic)
SCHEME_FLAG = 0               # Numerical scheme (0: Forward Euler; 1: Predictor-corrector)

Ncells = 32                   # Number of cells (including boundary cells '1' and 'Ncells' )
MegaSteps = 2                 # Number of simulation timesteps in millions
# Recommend at least 2 million steps for equilibrium systems, 20 million for non-equilibrium systems

# Set physical parameters for the system (iron bar)
kB = 1.38e-23       # Boltzmann constant (J/K)
mAtom = 9.27e-26    # Mass of iron atom (kg)
rho = 7870.0        # Mass density of iron (kg/m^3)
c_V = 450.0         # Specific heat capacity of iron (J/(kg K))
ThCond = 70.0       # Thermal conductivity of iron (W/(m K))
Length = 1.0e-8     # System length (m)
Area = (5.0e-9)^2   # System cross-sectional area (m^2)

# Set numerical parameters (time step, grid spacing, etc.).
if BC_FLAG == 0
    cellLo = 1
    cellHi = Ncells
    # For periodic BC, range of cells = { 1, ..., Ncells }
else
    cellLo = 2
    cellHi = Ncells - 1
    # For Dirichlet BC, range of cells = { 1, 2, ..., Ncells-1 }
end

dx = Length / (Ncells - 1)     # Grid size (m)
dV = Area * dx                 # Volume of grid cell (m^3)
xx = zeros(Float64, Ncells)
for ii = 1:Ncells
    xx[ii] = (ii - 1) * dx     # Cell center positions (m)
end

kappa = ThCond / (rho * c_V) # Coefficient in deterministic heat equation
# Coefficient in stochastic heat equation
if STOCH_FLAG == 1
    alpha = sqrt(2 * kB * kappa / (rho * c_V))
else
    alpha = 0.0 # Set alpha = 0 to turn off thermal fluctuations
end

stabilityFactor = 0.1                        # Numerical stability if stabilityFactor < 1.
dt = stabilityFactor * dx^2 / (2.0 * kappa)  # Timestep (s)

# Set initial conditions for temperature
Tref = 300.0        # Reference temperature (K)
if NONEQ_FLAG == 0
    T_Left = Tref
    T_Right = Tref
else
    Tdiff = 400.0   # Temperature difference across the system
    T_Left = Tref - Tdiff / 2.0
    T_Right = Tref + Tdiff / 2.0
end

# Standard deviation of temperature in a cell at the reference temperature
Tref_SD = sqrt(kB * Tref^2 / (rho * c_V * dV))

# Set initial temperature and its deterministic steady-state value
T = zeros(Float64, Ncells);
T0 = zeros(Float64, Ncells);
for ii = cellLo:cellHi
    T0[ii] = T_Left + (T_Right - T_Left) * (ii - 1) / (Ncells - 1) # Linear profile
    T[ii] = T0[ii]
    if PERTURB_FLAG == 1
        T[ii] += Tref_SD * randn() # Add random perturbation
    end
end

if BC_FLAG == 0
    T0[cellHi] = T0[1] # Copy first cell into last cell for periodic BCs
    T[cellHi] = T[1]
else
    T0[1] = T_Left
    T[1] = T_Left # Set values in first and last cells
    T0[end] = T_Right
    T[end] = T_Right
end

# Summarize system parameters and initial state
println("System is iron bar with about ", Int(floor(rho * Length * Area / mAtom)), " atoms")
println(
    "System length = ",
    Length * 1.0e9,
    " (nm); volume = ",
    (Length * Area) * 1.0e27,
    " (nm^3)",
)
println("Number of timesteps = ", MegaSteps, " million")
println("Time step = ", dt * 1.e15, " (fs)")
println("Number of cells = ", Ncells)
if BC_FLAG == 0
    println("** PERIODIC boundary conditions **")
else
    println("++ DIRICHLET boundary conditions ++")
end

if NONEQ_FLAG == 0
    println("Equilibrium temperature = ", Tref, " Kelvin")
else
    println("Fixed temperatures (K) = ", T_Left, " left; ", T_Right, " right")
end
println("At T = ", Tref, "K, standard deviation sqrt(< dT^2 >) = ", Tref_SD, "K")
if SCHEME_FLAG == 0
    println("-- Forward Euler Scheme --")
else
    println("== Predictor-Corrector Scheme ==")
end

# Plot temperature versus position
plot(xx, T, seriestype = :scatter, label = "Initial state (stars)")
plot!(xx, T0, seriestype = :steppre, label = "Steady state (dashed)")
plot!([xx[1]], [T[1]], seriestype = :scatter, label = "Boundary cells (squares)")
plot!([xx[end]], [T[end]], seriestype = :scatter)
xlabel!("Position (m)")
ylabel!("Temperature (K)")
title!("Initial state (stars), steady state (dashed), boundary cells (squares)")

# Initialize sampling plus misc. coefficients and arrays
Nsamp = 0                        # Count number of statistical samples
sumT = zeros(Float64, Ncells)    # Running sum of T_i ; used to compute mean
sumT2 = zeros(Float64, Ncells)   # Running sum of (T_i)^2 ; used to compute variance
sumTT = zeros(Float64, Ncells)   # Running sum for correlation T_i * T_iCorr

iCorr = Int(Ncells / 4) # Grid point used for correlation

# Coefficients used in the main loop calculations
coeffDetFE = kappa * dt / dx^2
coeffStoFE = alpha * dt / dx
coeffZnoise = 1.0 / sqrt(dt * dV)

# Arrays used in the main loop calculations
Determ = zeros(Float64, Ncells);
Stoch = zeros(Float64, Ncells);
Znoise = zeros(Float64, Ncells);
Tface = zeros(Float64, Ncells);
PreT = copy(T)             # Used by Predictor-Corrector scheme
Spectrum = zeros(Float64, Ncells)   # Used to compute structure factor S(k)

# Loop over the desired number of time steps.
NstepInner = 10                                       # Number of time steps (inner loop)
NstepOuter = Int(MegaSteps * 1000000 / NstepInner)    # Number of time steps (outer loop)
NskipOuter = NstepOuter / 10        # Number of outer steps to skip before sampling begins
NdiagOuter = NstepOuter / 20        # Number of outer steps between diagnostic outputs

@showprogress for iOuter = 1:NstepOuter     # Outer loop
    for iInner = 1:NstepInner # Inner loop
        # Deterministic update for temperature
        for ii = cellLo:cellHi
            Determ[ii] = coeffDetFE * (T[ii+1] + T[ii-1] - 2 * T[ii])
        end
        if BC_FLAG == 0
            Determ[1] = coeffDetFE * (T[2] + T[cellHi] - 2 * T[1])  # Periodic BC
        end

        # Generate random noise Z
        Znoise = coeffZnoise * randn(Ncells)
        if BC_FLAG == 0
            Znoise[end] = Znoise[1]   # Periodic BC
        end

        # Tface[ i ] is average between T of cells i-1 and i; value on the left face of cell i
        for ii = cellLo:cellHi
            Tface[ii] = 0.5 * (T[ii-1] + T[ii])
        end
        if BC_FLAG == 0
            Tface[cellHi] = Tface[1]   # Periodic BC
        else
            Tface[cellHi] = 0.5 * (T[cellHi-1] + T[cellHi])   # Dirichlet BC
        end

        # Stochastic update for temperature
        for ii = cellLo:cellHi
            Stoch[ii] = coeffStoFE * (Tface[ii+1] * Znoise[ii+1] - Tface[ii] * Znoise[ii])
        end

        if SCHEME_FLAG == 0
            # Forward Euler scheme
            for ii = cellLo:cellHi
                T[ii] += Determ[ii] + Stoch[ii]   # Total update for temperature
            end
        else
            # Predictor-Corrector scheme
            for ii = cellLo:cellHi
                PreT[ii] = T[ii] + Determ[ii] + Stoch[ii]     # Predictor step
            end
            if BC_FLAG == 0
                PreT[cellHi] = PreT[1]     # Periodic BC
            end
            # Corrector step
            for ii = cellLo:cellHi
                Determ[ii] = coeffDetFE * (PreT[ii+1] + PreT[ii-1] - 2 * PreT[ii])
            end
            if BC_FLAG == 0
                Determ[1] = coeffDetFE * (PreT[2] + PreT[cellHi] - 2 * PreT[1])  # Periodic BC     
            end
            for ii = cellLo:cellHi
                Tface[ii] = 0.5 * (PreT[ii-1] + PreT[ii])
            end
            if BC_FLAG == 0
                Tface[cellHi] = Tface[1]   # Periodic BC
            else
                Tface[cellHi] = 0.5 * (PreT[cellHi-1] + PreT[cellHi])
            end
            for ii = cellLo:cellHi
                Stoch[ii] =
                    coeffStoFE * (Tface[ii+1] * Znoise[ii+1] - Tface[ii] * Znoise[ii])
            end
            for ii = cellLo:cellHi
                T[ii] = 0.5 * (T[ii] + PreT[ii] + Determ[ii] + Stoch[ii])
            end
        end

        if BC_FLAG == 0
            T[cellHi] = T[1]     # Periodic BC
        end
    end # End of Inner loop

    # Take statistical sample
    if iOuter > NskipOuter
        Nsamp += 1
        for ii = cellLo:cellHi
            sumT[ii] += T[ii]            # Running sum for temperature average
            sumT2[ii] += T[ii]^2        # Running sum for temperature variance
            sumTT[ii] += T[ii] * T[iCorr]  # Running sum for temperature correlation
        end
    end
end

# Calculate average, variance, and correlation
aveT = zeros(Float64, Ncells)     # Average < T_i >
varT = zeros(Float64, Ncells)     # Variance < (T_i - <T_i>)^2 >
corrT = zeros(Float64, Ncells)    # Correlation < T_i T_iCorr >

for ii = cellLo:cellHi
    aveT[ii] = sumT[ii] / Nsamp
    varT[ii] = sumT2[ii] / Nsamp - aveT[ii]^2
end
for ii = cellLo:cellHi
    corrT[ii] = sumTT[ii] / Nsamp - aveT[ii] * aveT[iCorr]
end

# Plot temperature vs. x
scatter(
    xx[cellLo:cellHi],
    T[cellLo:cellHi],
    markershape = :star,
    label = "Final state (stars)",
)
scatter!(
    xx[cellLo:cellHi],
    aveT[cellLo:cellHi],
    label = "Average (circles)",
)
plot!(
    xx[cellLo:cellHi],
    T0[cellLo:cellHi],
    seriestype = :steppre,
    label = "Deterministic (line)",
)
xlabel!("Position (m)")
ylabel!("Temperature (K)")
title!("Final state (stars), Average (circles), Deterministic (line)")
