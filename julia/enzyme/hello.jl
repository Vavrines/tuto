using Enzyme

f(x) = x^2

autodiff(Reverse, f, Active, Active(1.0))
autodiff(Reverse, f, Const, Active(1.0))
autodiff(Reverse, f, Active, Const(1.0))
