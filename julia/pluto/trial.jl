### A Pluto.jl notebook ###
# v0.11.6

using Markdown
using InteractiveUtils

# ╔═╡ 653dc3c8-e3b0-11ea-2440-35437bf2164c
using Plots

# ╔═╡ 4ac3650c-e3b0-11ea-2267-a5f7f49b4824
x = 1:1:10;
y = randn(10);

# ╔═╡ 6fda5d64-e3b0-11ea-1ca1-833aa15e9e4d
plot(x, y)

# ╔═╡ Cell order:
# ╠═4ac3650c-e3b0-11ea-2267-a5f7f49b4824
# ╠═653dc3c8-e3b0-11ea-2440-35437bf2164c
# ╠═6fda5d64-e3b0-11ea-1ca1-833aa15e9e4d
