using Trixi, OrdinaryDiffEq

advectionvelocity = 1.0
equations = Trixi.LinearScalarAdvectionEquation1D(advectionvelocity)

initial_condition = initial_condition_gauss

surface_flux = flux_lax_friedrichs
solver = DGSEM(3, surface_flux)

coordinates_min = (-5,)
coordinates_max = (5,)
mesh = TreeMesh(
    coordinates_min,
    coordinates_max,
    initial_refinement_level = 4,
    n_cells_max = 30_000,
)

semi = SemidiscretizationHyperbolic(mesh, equations, initial_condition, solver)
