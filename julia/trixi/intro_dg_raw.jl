"""
Hand-written DG solver for the 1D advection equation.
"""

coordinates_min = -1.0 # minimum coordinate
coordinates_max = 1.0  # maximum coordinate

initial_condition_sine_wave(x) = 1.0 + 0.5 * sin(pi * x)

n_elements = 16 # number of elements

dx = (coordinates_max - coordinates_min) / n_elements # length of one element

using Trixi: LobattoLegendreBasis, LinearScalarAdvectionEquation1D, flux_lax_friedrichs

polydeg = 3 #= polynomial degree = N =#
basis = LobattoLegendreBasis(polydeg)

nodes = basis.nodes
weights = basis.weights

x = Matrix{Float64}(undef, length(nodes), n_elements)
for element = 1:n_elements
    x_l = coordinates_min + (element - 1) * dx + dx / 2
    for i = 1:length(nodes)
        ξ = nodes[i] # nodes in [-1, 1]
        x[i, element] = x_l + dx / 2 * ξ
    end
end

u0 = initial_condition_sine_wave.(x)

using Plots
plot(vec(x), vec(u0), label = "initial condition", legend = :topleft)

using LinearAlgebra
M = diagm(weights)

B = diagm([-1; zeros(polydeg - 1); 1])

D = basis.derivative_matrix

basis_N8 = LobattoLegendreBasis(8)

surface_flux = flux_lax_friedrichs
function rhs!(du, u, x, t)
    # Reset du and flux matrix
    du .= zero(eltype(du))
    flux_numerical = copy(du)

    # Calculate interface and boundary fluxes, $u^* = (u^*|_{-1}, 0, ..., 0, u^*|^1)^T$
    # Since we use the flux Lax-Friedrichs from Trixi.jl, we have to pass some extra arguments.
    # Trixi needs the equation we are dealing with and an additional `1`, that indicates the
    # first coordinate direction.
    equations = LinearScalarAdvectionEquation1D(1.0)
    for element = 2:n_elements-1
        # left interface
        flux_numerical[1, element] =
            surface_flux(u[end, element-1], u[1, element], 1, equations)
        flux_numerical[end, element-1] = flux_numerical[1, element]
        # right interface
        flux_numerical[end, element] =
            surface_flux(u[end, element], u[1, element+1], 1, equations)
        flux_numerical[1, element+1] = flux_numerical[end, element]
    end
    # boundary flux
    flux_numerical[1, 1] = surface_flux(u[end, end], u[1, 1], 1, equations)
    flux_numerical[end, end] = flux_numerical[1, 1]

    # Calculate surface integrals, $- M^{-1} * B * u^*$
    for element = 1:n_elements
        du[:, element] -= (M \ B) * flux_numerical[:, element]
    end

    # Calculate volume integral, $+ M^{-1} * D^T * M * u$
    for element = 1:n_elements
        flux = u[:, element]
        du[:, element] += (M \ transpose(D)) * M * flux
    end

    # Apply Jacobian from mapping to reference element
    for element = 1:n_elements
        du[:, element] *= 2 / dx
    end

    return nothing
end

using OrdinaryDiffEq
tspan = (0.0, 2.0)
ode = ODEProblem(rhs!, u0, tspan, x)

sol = solve(ode, RDPK3SpFSAL49(), abstol = 1.0e-6, reltol = 1.0e-6, save_everystep = false)

plot(
    vec(x),
    vec(sol.u[end]),
    label = "solution at t=$(tspan[2])",
    legend = :topleft,
    lw = 3,
)
