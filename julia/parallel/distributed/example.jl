"""
This is an illustrative example to use Distributed Arrays

ref: https://blog.csdn.net/iamzhtr/article/details/108565622?spm=1001.2014.3001.5501
"""

using Distributed

n = 4
addprocs(n)
@everywhere using DistributedArrays

@everywhere mutable struct Position{T}
    x::T
    y::T
    z::T
end

A = [Position(0.0, 0.0, 0.0) for i = 1:4, j = 1:4]
DA = distribute(A, procs = workers())

"""
Perform the computing among workers. Use @sync to synchronize all tasks.

With the master-slave mode, the matrix is handled in 4 workers.

(2 2 4 4
 2 2 4 4
 3 3 5 5
 3 3 5 5)
"""
@sync for pid in workers()
    # Send the task to the remote worker whose PID = pid. The task is wrapped in a "begin" block.
    @spawnat pid begin
        # You can read any element of DA directly on any worker.
        for a in DA
            #            println(a.x)
        end

        # You can modify the element of DA only when the element is stored on the current worker.
        for a in DA
            a.x = myid()
        end

        # To modify the element, it is highly recommended to use localpart() to access the local part of DA on the remote worker.
        for a in localpart(DA)
            a.y = myid()
        end

        # Or use localindices().
        r1, r2 = localindices(DA)
        for i in r1, j in r2
            DA[i, j].z = myid()
        end
    end
end

# You can read DA on any worker.
display(DA)
