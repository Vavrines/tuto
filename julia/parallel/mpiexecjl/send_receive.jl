"""
MPI send/receive example
based on master/slave mode

"""

using MPI

MPI.Init()

comm = MPI.COMM_WORLD
size = MPI.Comm_size(comm) # total node number
rank = MPI.Comm_rank(comm) # current node rank

dst = mod(rank + 1, size) # destination
src = mod(rank - 1, size) # source

N = 3

send_mesg = randn(N)
recv_mesg = Array{Float64}(undef, N)

if rank == 0
    MPI.send(send_mesg, dst, 32, comm) # tag should be consistent in sender & receiver
else
    recv_mesg = MPI.recv(src, 32, comm)[1] # tag should be consistent in sender & receiver
end

rank == 0 && @show send_mesg
rank == 1 && @show recv_mesg
