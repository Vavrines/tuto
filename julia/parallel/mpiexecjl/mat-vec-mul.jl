"""
We reproduce a parallel matrix-vector multiplication from the mpi4py example
(https://github.com/jbornschein/mpi4py-examples/blob/master/07-matrix-vector-product).

The code compute the multiplication with the form v(t+1) = M * v(t).
The length of v must be an integer multiple of MPI's comm.size.
v is initialized to be zero except of v[1] = 1, and M is a "off-by-one" diagonal matrix M[i+1, i] = 1, i.e.,
(0
1   0
    1   0
        1   0)
Therefore, after n iterations, v should be an all-zero vector except for v[n+1] = 1.

In this example every MPI process is responsible for calculating a different portion of v. 
Every process only knows the stripe of M, that is relevant for it's calculation. 
At the end of every iteration, Allgather function is used to distribute the partial vectors v to all other processes.

"""

using MPI, Test

MPI.Init()
comm = MPI.COMM_WORLD
size = MPI.Comm_size(comm)
rank = MPI.Comm_rank(comm)

N = 100
iter = 10

rank == 0 && println("Running $size parallel MPI processes")

my_size = N ÷ size
N = size * my_size
my_offset = rank * my_size

vec = [1.0; zeros(N - 1)]

my_M = zeros(my_size, N)
for i = 2:my_size
    j = (my_offset + i - 1) % N
    my_M[i, j] = 1.0
end
MPI.Barrier(comm)

for t = 1:iter
    my_new_vec = my_M * vec
    MPI.Allgather!(my_new_vec, vec, comm)

    @test abs(vec[t+1] - 1.0) < 0.01
end
MPI.Barrier(comm)
