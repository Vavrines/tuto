using MPI, Test

MPI.Init()
comm = MPI.COMM_WORLD
rank = MPI.Comm_rank(comm)
size = MPI.Comm_size(comm)
root = 0

if rank == root
    print(" Running on $(size) processes\n")
end
MPI.Barrier(comm)

N = 3
if MPI.Comm_rank(comm) == root
    A = [rand() for i = 1:N]
else
    A = Array{Float64}(undef, N)
end

MPI.Bcast!(A, root, comm)
println("rank = $(rank), A = $A")

dst = mod(rank + 1, size)
src = mod(rank - 1, size)

send_mesg = ArrayType{Float64}(undef, N)
recv_mesg = ArrayType{Float64}(undef, N)
recv_mesg_expected = ArrayType{Float64}(undef, N)
fill!(send_mesg, Float64(rank))
fill!(recv_mesg_expected, Float64(src))

rreq = MPI.Irecv!(recv_mesg, src, src + 32, comm)
sreq = MPI.Isend(send_mesg, dst, rank + 32, comm)

stats = MPI.Waitall([sreq, rreq], MPI.Status)

@test MPI.Get_source(stats[2]) == src
@test MPI.Get_tag(stats[2]) == src + 32
@test recv_mesg == recv_mesg_expected
