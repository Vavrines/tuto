using PyPlot
cd(@__DIR__)

begin
    include("tools.jl")
    close("all")
end

begin
    nx = 50
    uconst = 1.0
    type = pqm # pcom, plm, ppm, pcm, pqm
    islimited = false
    #islimited = true

    grid = Grid1d(nx; xmin = 0.0, xmax = 1.0)
    set_IC!(grid; type = :gaussian, uconst)

    nstep = 30
    CFL = 0.5
    dt = CFL * grid.dx / uconst

    ρinit = copy(grid.ρ)
    scatter(grid.x, ρinit, label = "cell center")
end

begin
    xp = range(grid.xmin, grid.xmax - 1e-4, length = 401)
    ap = zeros(size(xp))
    for i in eachindex(xp)
        ap[i] = interpolate(xp[i], grid; type, islimited)
    end
    scatter(xp, ap, marker = ".", label = uppercase(string(type)))
end

begin
    advance_SLICE!(grid, type, nstep, dt, islimited)

    for i in eachindex(xp)
        ap[i] = interpolate(xp[i], grid; type, islimited)
    end
    scatter(xp, ap, marker = ".", label = uppercase(string(type)))
end

begin
    ishift = round(Int, nstep * dt * uconst / grid.dx)
    circshift!(ρinit, ishift)

    plot(grid.x, ρinit, label = "analytical")
    scatter(grid.x, grid.ρ, label = "cell center new")
    legend()
end

display(gcf())
