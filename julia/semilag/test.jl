using Plots

cd(@__DIR__)
include("tools.jl")

begin
    nx = 50
    uconst = 1.0
    type = plm # pcom, plm, ppm, pcm, pqm
    islimited = false
    #islimited = true

    grid = Grid1d(nx; xmin = 0.0, xmax = 1.0)
    set_IC!(grid; type = :gaussian, uconst)
    ρ0 = copy(grid.ρ)

    nstep = 30
    CFL = 0.5
    dt = CFL * grid.dx / uconst

    ishift = round(Int, nstep * dt * uconst / grid.dx)
    ρ1 = deepcopy(ρ0)
    circshift!(ρ1, ishift)
end

#advance_SLICE!(grid, type, nstep, dt, islimited)

poly = PiecewisePolynomials(grid; type, islimited)
get_departure_points!(grid, dt)



begin
    scatter(grid.x, ρ0, label = "cell center")
    plot!(grid.x, ρ1, label = "analytical")
    scatter!(grid.x, grid.ρ, label = "cell center new")
end
