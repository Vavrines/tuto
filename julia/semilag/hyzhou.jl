# Conservative remapping of scalar variables in 1D.
# Semi-Lagrangian methods based on finite difference suffer from lack of conservation.
# By using the idea from finite volume methods, the conservation property can be ensured.
#
# Steps:
# 1. Construct piecewise polynomials.
# 1. Find departure points.
# 2. Integrate in LCVs and remap to ECVs.
#
# My current implementation of the SLICE scheme does not look right so far: only PCoM shows
# no dispersion error!!! Where does the dispersion error originate?
# 1. Monoticity: PCoM is the only monotonic scheme in this family.
# 2. Departure point calculation?
# Demonstration of problems:
# 1. nx = 20, x ∈ [0.0, 1.0], nstep = 10, CFL = 1.3, type = plm, typeinit = :single
# --> the peak moves 1 cell ahead of the analytical solution. If the type is switched to
# higher orders, the peak moves further ahead.
#
# Reference:
# Semi-Lagrangian Inherently Conserving and Efficient (SLICE) scheme,
# Mohamed Zerroukat, Nigel Wood, and Andrew Staniforth, Met Office UK, 2002
#
# A monotonic and positive–definite filter for a Semi-Lagrangian Inherently Conserving and
# Efficient (SLICE) scheme,
# Mohamed Zerroukat, Nigel Wood, and Andrew Staniforth, Met Office UK, 2004
#
# A high-order finite volume remapping scheme for nonuniform grids:
# The piecewise quartic method (PQM),
# Laurent White and Alistair Adcroft, Princeton, 2008
#
# Piecewise polynomial interpolations:
# 0th order, Piecewise Constant Method (PCoM), Gudunov type
# 1st order, Piecewise Linear Method (PLM), van Leer 1977
# 2nd order, Piecewise Parabolic Method (PPM), Colella & Woodward, 1984
# 3rd order, Piecewise Cubic Method (PCM), Zerroukat 2002
# 4th order, Piecewise Quartic Method (PQM), White & Adcroft, 2008
#
# Hongyang Zhou, hyzhou@umich.edu, 2023/09/01

using DataInterpolations
using CircularArrays
using Statistics: norm
using PyPlot

struct Grid1d{T,Tx<:AbstractVector{T}}
    "number of cells"
    nx::Int
    "minimum location"
    xmin::T
    "maximum location"
    xmax::T
    "uniform length of cells"
    dx::T
    "cell-centered coordinates"
    x::Tx
    "node coordinates, including all left and right edges"
    xn::Tx
    "departure point shifts measured in cell size"
    xs::Vector{T}
    "cell indexes to which departure points belong"
    xdCell::Vector{Int}
    "local cell coordinates for each departure point"
    ξ::Vector{T}
    "scalar variable"
    ρ::CircularVector{T,Vector{T}}
    "scalar integral over each cell, used as intermediate variable"
    mass::Vector{T}
    "velocity"
    u::Vector{T}

    function Grid1d(
        nx::Int;
        xmin::T = 0.0,
        xmax::T = 1.0,
        ρ::CircularVector = CircularArray(0.0, nx),
        u::Vector = zeros(nx),
    ) where {T}
        # physical coords: cell-centered, left and right edges
        dx = (xmax - xmin) / nx
        x = range(xmin + 0.5dx, xmax - 0.5dx, step = dx)
        xn = range(xmin, xmax, step = dx)
        xs = zeros(T, nx + 1)
        xdCell = zeros(Int, nx + 1)
        ξ = zeros(T, nx + 1)

        new{T,typeof(x)}(nx, xmin, xmax, dx, x, xn, xs, xdCell, ξ, ρ, zeros(T, nx), u)
    end
end

@enum InterpolationMethod begin
    pcom # constant,  0th order
    plm  # linear,    1st order
    ppm  # parabolic, 2nd order
    pcm  # cubic,     3rd order
    pqm  # quartic,   4th order
end

"""
Explicit 3rd order estimate of edge values, averaging over two cell-centered polynomials at
`j`.
Ref: [White+ 2008], Eq.(9)
"""
function h3(ρ::AbstractVector, j::Int)
    ρL = (2ρ[j-1] + 5ρ[j] - ρ[j+1]) / 6
    ρR = (-ρ[j-1] + 5ρ[j] + 2ρ[j+1]) / 6

    return ρL, ρR
end

"""
Explicit 4th order estimate of edge values.
Ref: [White+ 2008], Eq.(10)
"""
function h4(ρ::AbstractVector, j::Int)
    ρL = (-ρ[j-2] + 7ρ[j-1] + 7ρ[j] - ρ[j+1]) / 12 # edge-centered at j-1/2
    ρR = (-ρ[j-1] + 7ρ[j] + 7ρ[j+1] - ρ[j+2]) / 12 # edge-centered at j+1/2

    return ρL, ρR
end

"""
Explicit 5th order estimate of edge values.
Ref: [White+ 2008], Eq.(11)
"""
function h5(ρ::AbstractVector, j::Int)
    ρL = (-3ρ[j-2] + 27ρ[j-1] + 47ρ[j] - 13ρ[j+1] + 2ρ[j+2]) / 60
    ρR = (2ρ[j-2] - 13ρ[j-1] + 47ρ[j] + 27ρ[j+1] - 3ρ[j+2]) / 60

    return ρL, ρR
end

"""
Explicit 6th order estimate of edge values.
Ref: [White+ 2008], Eq.(12)
"""
function h6(ρ::AbstractVector, j::Int)
    ρL = (ρ[j-3] - 8ρ[j-2] + 37ρ[j-1] + 37ρ[j] - 8ρ[j+1] + ρ[j+2]) / 60
    ρR = (ρ[j-2] - 8ρ[j-1] + 37ρ[j] + 37ρ[j+1] - 8ρ[j+2] + ρ[j+3]) / 60

    return ρL, ρR
end

"""
Explicit 4th order estimate of edge slopes, without dividing cell size.
Ref: [White+ 2008], Eq.(14)
"""
function h4′(ρ::AbstractVector, j::Int)
    ρL′ = (15 * (ρ[j] - ρ[j-1]) - (ρ[j+1] - ρ[j-2])) / 12
    ρR′ = (15 * (ρ[j+1] - ρ[j]) - (ρ[j+2] - ρ[j-1])) / 12

    return ρL′, ρR′
end

"""
Explicit 5th order estimate of edge slopes, without dividing cell size.
Ref: [White+ 2008], Eq.(15)
"""
function h5′(ρ::AbstractVector, j::Int)
    ρL′ = (245 * (ρ[j] - ρ[j-1]) - 25 * (ρ[j+1] - ρ[j-2]) + 2 * (ρ[j+2] - ρ[j-3])) / 180
    ρR′ = (245 * (ρ[j+1] - ρ[j]) - 25 * (ρ[j+2] - ρ[j-1]) + 2 * (ρ[j+3] - ρ[j-2])) / 180

    return ρL′, ρR′
end

struct PiecewisePolynomials{T<:AbstractFloat}
    c::CircularArray{T,2,Matrix{T}}
end

function PiecewisePolynomials(
    grid::Grid1d;
    type::InterpolationMethod = ppm,
    islimited::Bool = false,
)
    (; nx, ρ) = grid

    if type == pcom
        c = CircularArray(zero(eltype(ρ)), (1, nx))
        set_coefs_pcom!(ρ, c)
    elseif type == plm
        c = CircularArray(zero(eltype(ρ)), (2, nx))
        if !islimited
            set_coefs_plm!(ρ, c)
        else
            set_coefs_plm_limited!(ρ, c)
        end
    elseif type == ppm
        c = CircularArray(zero(eltype(ρ)), (3, nx))
        set_coefs_ppm!(ρ, c)
    elseif type == pcm
        c = CircularArray(zero(eltype(ρ)), (4, nx))
        set_coefs_pcm!(ρ, c)
    elseif type == pqm
        c = CircularArray(zero(eltype(ρ)), (5, nx))
        if !islimited
            set_coefs_pqm!(ρ, c)
        else
            set_coefs_pqm_limited!(ρ, c)
        end
    end

    return PiecewisePolynomials(c)
end

function set_coefs_pcom!(ρ, c)
    for i in axes(c, 2)
        c[1, i] = ρ[i]
    end
end

function set_coefs_plm!(ρ, c)
    for i in axes(c, 2)
        ρL, ρR = h3(ρ, i)
        c[1, i] = ρL
        c[2, i] = ρR - ρL
    end
end

function set_coefs_ppm!(ρ, c)
    for i in axes(c, 2)
        #ρL, ρR = h3(ρ, i)
        ρL, ρR = h4(ρ, i)

        c[1, i] = ρL
        c[2, i] = 6ρ[i] - 4ρL - 2ρR
        c[3, i] = 3(ρL + ρR - 2ρ[i])
    end
end

function set_coefs_pcm!(ρ, c)
    for i in axes(c, 2)
        ρL, ρR = h4(ρ, i)
        δρ = ρR - ρL # reduces to PPM

        c[1, i] = ρL
        c[2, i] = 6ρ[i] - 6ρL - 2δρ
        c[3, i] = 9ρL - 3ρR - 6ρ[i] + 6δρ
        c[4, i] = -4ρL + 4ρR - 4δρ
    end
end

function set_coefs_pqm!(ρ, c)
    for i in axes(c, 2)
        ρL, ρR = h6(ρ, i)
        ρL′, ρR′ = h5′(ρ, i)

        c[1, i] = ρL
        c[2, i] = ρL′
        c[3, i] = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
        c[4, i] = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
        c[5, i] = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)
    end
end

function set_coefs_plm_limited!(ρ, c)
    for i in axes(c, 2)
        ρL, ρR = h3(ρ, i)

        σL = 2 * (ρ[i] - ρ[i-1])
        σR = 2 * (ρ[i+1] - ρ[i])
        σC = 0.5 * (ρ[i+1] - ρ[i-1])

        σ = if σL * σR > 0
            sign(σC) * min(abs(σL), abs(σR), abs(σC))
        else
            0.0
        end
        # left edge correction
        if (ρ[i-1] - ρL) * (ρL - ρ[i]) < 0
            ρL = ρ[i] - sign(σ) * min(0.5 * abs(σ), abs(ρL - ρ[i]))
        end
        # right edge correction
        if (ρ[i+1] - ρR) * (ρR - ρ[i]) < 0
            ρR = ρ[i] + sign(σ) * min(0.5 * abs(σ), abs(ρR - ρ[i]))
        end

        c[1, i] = ρ[i] - 0.5 * σ
        c[2, i] = σ
    end
end

function set_coefs_pqm_limited!(ρ, c)
    for i in axes(c, 2)
        ρL, ρR = h6(ρ, i)
        ρL′, ρR′ = h5′(ρ, i)

        # Check whether the edge values are bounded by the neighboring cell averages.
        σ, σL, σR, ρL, ρR = limit_slope_edge(ρ, ρL, ρR, i)
        if σ == 0.0 # Check whether the current cell average is an extremum.
            c[1, i] = ρ[i]
            c[2:5, i] .= 0.0
            continue
        end
        # Check whether the edge slopes are consistent with σ
        if sign(σ) != sign(ρL′)
            ρL′ = σ
        end
        if sign(σ) != sign(ρR′)
            ρR′ = σ
        end

        c[1, i] = ρL
        c[2, i] = ρL′
        c[3, i] = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
        c[4, i] = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
        c[5, i] = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)
        # Check for the existence of inflexion points in [0, 1].   
        b0 = 2 * c[3, i]
        b1 = 6 * c[4, i]
        b2 = 12 * c[5, i]

        tmp = b1^2 - 4 * b0 * b2
        if tmp < 0
            continue
        end
        ξ1 = 0.5 * (-b1 + √tmp) / b2
        ξ2 = 0.5 * (-b1 - √tmp) / b2

        σ1 = c[2, i] + 2 * c[3, i] * ξ1 + 3 * c[4, i] * ξ1^2 + 4 * c[5, i] * ξ1^3
        σ2 = c[2, i] + 2 * c[3, i] * ξ2 + 3 * c[4, i] * ξ2^2 + 4 * c[5, i] * ξ2^3

        isConsistent1 = (0 ≤ ξ1 ≤ 1 && sign(σ) == sign(σ1))
        isConsistent2 = (0 ≤ ξ2 ≤ 1 && sign(σ) == sign(σ2))

        if !(0 ≤ ξ1 ≤ 1) && !(0 ≤ ξ2 ≤ 1) # no inflexion points
            continue
        elseif isConsistent1 && isConsistent2 # 2 inflexion pts
            continue
        elseif isConsistent1 && !(0 ≤ ξ2 ≤ 1) # 1 inflexion point ξ1
            continue
        elseif !(0 ≤ ξ1 ≤ 1) && isConsistent2 # 1 inflexion point ξ2
            continue
        else
            if sign(σ) == sign(σ1) == sign(σ2)
                continue
            else
                if abs(σL) ≤ abs(σR) # collapse inflexion points on the left edge
                    ρL′ = (10ρ[i] - 2ρR - 8ρL) / 3
                    ρR′ = -10ρ[i] + 6ρR + 4ρL
                    if ρL′ * σ < 0
                        ρL′ = 0.0
                        ρR = 5ρ[i] - 4ρL
                        ρR′ = 20 * (ρ[i] - ρL)
                    end
                    if ρR′ * σ < 0
                        ρR′ = 0.0
                        ρL = 2.5ρ[i] - 1.5ρR
                        ρL′ = 10 / 3 * (-ρ[i] + ρR)
                    end
                else # collapse inflexion points on the right edge
                    ρL′ = 10ρ[i] - 4ρR - 6ρL
                    ρR′ = (-10ρ[i] + 8ρR + 2ρL) / 3
                    if ρL′ * σ < 0
                        ρL′ = 0.0
                        ρR = 2.5ρ[i] - 1.5ρL
                        ρR′ = 10 / 3 * (ρ[i] - ρL)
                    end
                    if ρR′ * σ < 0
                        ρR′ = 0.0
                        ρL = 5ρ[i] - 4ρR
                        ρL′ = 20 * (-ρ[i] + ρR)
                    end
                end
            end
        end

        c[1, i] = ρL
        c[2, i] = ρL′
        c[3, i] = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
        c[4, i] = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
        c[5, i] = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)
    end
end

function plm_unlimited(ρ, i, ξ)
    ρL, ρR = h3(ρ, i)
    c0 = ρL
    c1 = ρR - ρL

    return c0 + c1 * ξ
end

function ppm_unlimited(ρ, i, ξ)
    ρL, ρR = h4(ρ, i)

    c0 = ρL
    c1 = 6ρ[i] - 4ρL - 2ρR
    c2 = 3(ρL + ρR - 2ρ[i])

    return c0 + c1 * ξ + c2 * ξ^2
end

function pcm_unlimited(ρ, i, ξ)
    ρL, ρR = h4(ρ, i)
    δρ = ρR - ρL
    #h4′(ρ, j, dx)

    c0 = ρL
    c1 = 6ρ[i] - 6ρL - 2δρ
    c2 = 9ρL - 3ρR - 6ρ[i] + 6δρ
    c3 = -4ρL + 4ρR - 4δρ

    return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3
end

function pqm_unlimited(ρ, i, ξ)
    ρL, ρR = h6(ρ, i)
    ρL′, ρR′ = h5′(ρ, i)

    c0 = ρL
    c1 = ρL′
    c2 = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
    c3 = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
    c4 = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)

    return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
end

function plm_limited(ρ, i, ξ)
    ρL, ρR = h3(ρ, i)

    σ, _, _, _, _ = limit_slope_edge(ρ, ρL, ρR, i)

    c0 = ρ[i] - 0.5 * σ
    c1 = σ

    return c0 + c1 * ξ
end

function limit_slope_edge(ρ, ρL, ρR, i)
    σL = 2 * (ρ[i] - ρ[i-1])
    σR = 2 * (ρ[i+1] - ρ[i])
    σC = 0.5 * (ρ[i+1] - ρ[i-1])

    σ = if σL * σR > 0
        sign(σC) * min(abs(σL), abs(σR), abs(σC))
    else
        0.0
    end
    # left edge correction
    if (ρ[i-1] - ρL) * (ρL - ρ[i]) < 0
        ρL = ρ[i] - sign(σ) * min(0.5 * abs(σ), abs(ρL - ρ[i]))
    end
    # right edge correction
    if (ρ[i+1] - ρR) * (ρR - ρ[i]) < 0
        ρR = ρ[i] + sign(σ) * min(0.5 * abs(σ), abs(ρR - ρ[i]))
    end

    return σ, σL, σR, ρL, ρR
end

"Limited PQM, Section 2.6 [White+, 2008]"
function pqm_limited(ρ, i, ξ)
    ρL, ρR = h6(ρ, i)
    ρL′, ρR′ = h5′(ρ, i)

    # Check whether the current cell average is an extremum, and
    # the edge values are bounded by the neighboring cell averages.
    σ, σL, σR, ρL, ρR = limit_slope_edge(ρ, ρL, ρR, i)
    if σ == 0.0
        return ρ[i]
    end
    # Check whether the edge slopes are consistent with σ
    if sign(σ) != sign(ρL′)
        ρL′ = σ
    end
    if sign(σ) != sign(ρR′)
        ρR′ = σ
    end
    # Check for the existence of inflexion points in [0, 1].
    c0 = ρL
    c1 = ρL′
    c2 = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
    c3 = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
    c4 = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)

    b0 = 2 * c2
    b1 = 6 * c3
    b2 = 12 * c4
    tmp = b1^2 - 4 * b0 * b2
    if tmp < 0
        return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
    end
    ξ1 = 0.5 * (-b1 + √tmp) / b2
    ξ2 = 0.5 * (-b1 - √tmp) / b2

    σ1 = c1 + 2 * c2 * ξ1 + 3 * c3 * ξ1^2 + 4 * c4 * ξ1^3
    σ2 = c1 + 2 * c2 * ξ2 + 3 * c3 * ξ2^2 + 4 * c4 * ξ2^3

    isConsistent1 = (0 ≤ ξ1 ≤ 1 && sign(σ) == sign(σ1))
    isConsistent2 = (0 ≤ ξ2 ≤ 1 && sign(σ) == sign(σ2))

    if !(0 ≤ ξ1 ≤ 1) && !(0 ≤ ξ2 ≤ 1) # no inflexion points
        return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
    elseif isConsistent1 && isConsistent2 # 2 inflexion pts
        return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
    elseif isConsistent1 && !(0 ≤ ξ2 ≤ 1) # 1 inflexion point ξ1
        return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
    elseif !(0 ≤ ξ1 ≤ 1) && isConsistent2 # 1 inflexion point ξ2
        return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
    else
        if sign(σ) == sign(σ1) == sign(σ2)
            return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
        else
            if abs(σL) ≤ abs(σR) # collapse inflexion points on the left edge
                ρL′ = (10ρ[i] - 2ρR - 8ρL) / 3
                ρR′ = -10ρ[i] + 6ρR + 4ρL
                if ρL′ * σ < 0
                    ρL′ = 0.0
                    ρR = 5ρ[i] - 4ρL
                    ρR′ = 20 * (ρ[i] - ρL)
                end
                if ρR′ * σ < 0
                    ρR′ = 0.0
                    ρL = 2.5ρ[i] - 1.5ρR
                    ρL′ = 10 / 3 * (-ρ[i] + ρR)
                end
            else # collapse inflexion points on the right edge
                ρL′ = 10ρ[i] - 4ρR - 6ρL
                ρR′ = (-10ρ[i] + 8ρR + 2ρL) / 3
                if ρL′ * σ < 0
                    ρL′ = 0.0
                    ρR = 2.5ρ[i] - 1.5ρL
                    ρR′ = 10 / 3 * (ρ[i] - ρL)
                end
                if ρR′ * σ < 0
                    ρR′ = 0.0
                    ρL = 5ρ[i] - 4ρR
                    ρL′ = 20 * (-ρ[i] + ρR)
                end
            end

            c0 = ρL
            c1 = ρL′
            c2 = 30ρ[i] - 12ρR - 18ρL + 1.5ρR′ - 4.5ρL′
            c3 = -60ρ[i] + 6ρL′ - 4ρR′ + 28ρR + 32ρL
            c4 = 30ρ[i] + 2.5 * (ρR′ - ρL′) - 15 * (ρL + ρR)

            return c0 + c1 * ξ + c2 * ξ^2 + c3 * ξ^3 + c4 * ξ^4
        end
    end
end

function reconstruct!(grid::Grid1d, poly::PiecewisePolynomials, islimited::Bool = false)
    ρ = grid.ρ
    c = poly.c

    order = size(c, 1) - 1
    if order == 0
        set_coefs_pcom!(ρ, c)
    elseif order == 1
        if !islimited
            set_coefs_plm!(ρ, c)
        else
            set_coefs_plm_limited!(ρ, c)
        end
    elseif order == 2
        set_coefs_ppm!(ρ, c)
    elseif order == 3
        set_coefs_pcm!(ρ, c)
    elseif order == 4
        if !islimited
            set_coefs_pqm!(ρ, c)
        else
            set_coefs_pqm_limited!(ρ, c)
        end
    end

    return
end

"Interpolate the piecewise polynomials of `type` constructed from `grid` at `x`."
function interpolate(
    x::AbstractFloat,
    grid::Grid1d;
    type::InterpolationMethod = ppm,
    islimited::Bool = false,
)
    (; xn, dx, ρ) = grid

    i = Int((x - xn[1]) ÷ dx) + 1
    ξ = ((x - xn[1]) % dx) / dx

    if type == pcom
        return ρ[i]
    elseif type == plm
        if !islimited
            return plm_unlimited(ρ, i, ξ)
        else
            return plm_limited(ρ, i, ξ)
        end
    elseif type == ppm
        return ppm_unlimited(ρ, i, ξ)
    elseif type == pcm
        return pcm_unlimited(ρ, i, ξ)
    elseif type == pqm
        if !islimited
            return pqm_unlimited(ρ, i, ξ)
        else
            return pqm_limited(ρ, i, ξ)
        end
    end

end

"""
Obtain the departure points shifts measured in `dx` within `dt` using the simplest method.
If the velocity is not a constant, we should use more complicated methods as discussed in
Treatment of vector equations in deep-atmosphere, semi-Lagrangian models.
II: Kinematic equation,
N. Wood, A. A. White and A. Staniforth, 2010
"""
function get_departure_points!(grid::Grid1d, dt::AbstractFloat)
    # estimate velocity at x
    #interp = CubicSpline(grid.u, grid.x)
    interp = LinearInterpolation(grid.u, grid.x)
    ux = interp(grid.xn)
    @. grid.xs = -ux * dt / grid.dx

    return
end

"Return departure points cell indexes and local coordinates."
function locate_cell_indexes!(grid::Grid1d)
    (; nx, xs, ξ, xdCell) = grid

    for i = 1:nx+1
        j = floor(Int, xs[i])
        xdCell[i] = i + j
        ξ[i] = xs[i] - j
    end

    return
end

"""
Remap mass from the Lagrangian Control Volume (LCV) to the Eulerian Control Volume (ECV).
By assuming uniform grid, we can directly obtain density by canceling divisions and
multiplications in the integration.
# Arguments
- `grid::Grid1d`: the uniform mesh.
- `poly::PiecewisePolynomials`: the piecewise polynomials for reconstruction.
# Keywords
- `type::InterpolationMethod`: type of interpolation.
"""
function remap!(grid::Grid1d, poly::PiecewisePolynomials; type::InterpolationMethod = ppm)
    (; nx, xdCell, ξ, ρ, mass) = grid
    c = poly.c

    if type == pcom
        for i = 1:nx
            l, m, x1 = xdCell[i], xdCell[i+1], 1 - ξ[i]

            mass[i] = if l < m
                part2 = sum(@view ρ[l+1:m-1])
                part1 = c[1, l] * x1
                part3 = c[1, m] * ξ[i+1]
                part1 + part2 + part3
            else
                c[1, m] * (ξ[i+1] - ξ[i])
            end
        end
    elseif type == plm
        for i = 1:nx
            l, m, x1 = xdCell[i], xdCell[i+1], 1 - ξ[i]

            mass[i] = if l < m
                part2 = sum(@view ρ[l+1:m-1])
                part1 = c[1, l] * x1 + 0.5 * c[2, l] * x1^2
                part3 = c[1, m] * ξ[i+1] + 0.5 * c[2, m] * ξ[i+1]^2
                part1 + part2 + part3
            else
                c[1, m] * (ξ[i+1] - ξ[i]) + 0.5 * c[2, m] * (ξ[i+1]^2 - ξ[i]^2)
            end
        end
    elseif type == ppm
        for i = 1:nx
            l, m, x1 = xdCell[i], xdCell[i+1], 1 - ξ[i]

            mass[i] = if l < m
                part2 = sum(@view ρ[l+1:m-1])
                part1 = c[1, l] * x1 + 0.5 * c[2, l] * x1^2 + 1 / 3 * c[3, l] * x1^3
                part3 =
                    c[1, m] * ξ[i+1] + 0.5 * c[2, m] * ξ[i+1]^2 + 1 / 3 * c[3, m] * ξ[i+1]^3
                part1 + part2 + part3
            else # l == m
                c[1, m] * (ξ[i+1] - ξ[i]) +
                0.5 * c[2, m] * (ξ[i+1]^2 - ξ[i]^2) +
                1 / 3 * c[3, m] * (ξ[i+1]^3 - ξ[i]^3)
            end
        end
    elseif type == pcm
        for i = 1:nx
            l, m, x1 = xdCell[i], xdCell[i+1], 1 - ξ[i]

            mass[i] = if l < m
                part2 = sum(@view ρ[l+1:m-1])
                part1 =
                    c[1, l] * x1 +
                    0.5 * c[2, l] * x1^2 +
                    1 / 3 * c[3, l] * x1^3 +
                    0.2 * c[4, l] * x1^4
                part3 =
                    c[1, m] * ξ[i+1] +
                    0.5 * c[2, m] * ξ[i+1]^2 +
                    1 / 3 * c[3, m] * ξ[i+1]^3 +
                    0.25 * c[4, m] * ξ[i+1]^4
                part1 + part2 + part3
            else
                c[1, m] * (ξ[i+1] - ξ[i]) +
                0.5 * c[2, m] * (ξ[i+1]^2 - ξ[i]^2) +
                1 / 3 * c[3, m] * (ξ[i+1]^3 - ξ[i]^3) +
                0.25 * c[4, m] * (ξ[i+1]^4 - ξ[i]^4)
            end
        end
    elseif type == pqm
        for i = 1:nx
            l, m, x1 = xdCell[i], xdCell[i+1], 1 - ξ[i]

            mass[i] = if l < m
                part2 = sum(@view ρ[l+1:m-1])
                part1 =
                    c[1, l] * x1 +
                    0.5 * c[2, l] * x1^2 +
                    1 / 3 * c[3, l] * x1^3 +
                    0.25 * c[4, l] * x1^4 +
                    0.2 * c[5, l] * x1^5
                part3 =
                    c[1, m] * ξ[i+1] +
                    0.5 * c[2, m] * ξ[i+1]^2 +
                    1 / 3 * c[3, m] * ξ[i+1]^3 +
                    0.25 * c[4, m] * ξ[i+1]^4 +
                    0.2 * c[5, m] * ξ[i+1]^5
                part1 + part2 + part3
            else
                c[1, m] * (ξ[i+1] - ξ[i]) +
                0.5 * c[2, m] * (ξ[i+1]^2 - ξ[i]^2) +
                1 / 3 * c[3, m] * (ξ[i+1]^3 - ξ[i]^3) +
                0.25 * c[4, m] * (ξ[i+1]^4 - ξ[i]^4) +
                0.2 * c[5, m] * (ξ[i+1]^5 - ξ[i]^5)
            end
        end
    end
    # Avoid dx multiplication and division via assuming constant dx
    ρ .= mass

    return
end

"Test function from [Zerroukat+ 2004] for SLICE-1D."
test_func(c, x) =
    (tanh(c[1] * (x - c[2])) + tanh(c[3] * (x - c[4]))) *
    (1 + c[5] * sinpi(2 * c[6] * x)) *
    (1 + c[7] * sin(2π * c[8] * x - c[9])) +
    (tanh(c[10] * (x - c[11])) + tanh(-c[10] * (x - c[12]))) +
    c[13]

"Set initial densities and velocities."
function set_IC!(grid::Grid1d; type = :tophat, uconst = 1.0)
    (; ρ, u, x) = grid
    ρ .= 0.0
    if type == :tophat
        for i in eachindex(ρ)
            if grid.nx / 3 ≤ i ≤ 2 / 3 * grid.nx
                ρ[i] = 1.0
            else
                ρ[i] = -1.0
            end
        end
    elseif type == :sin
        @. ρ = sin(2π * (grid.x) / (grid.xmax - grid.xmin))
    elseif type == :gaussian
        xmid = 0.5 * (grid.xmin + grid.xmax)
        σ = 0.1
        @. ρ = exp(-((x - xmid) / σ)^2)
    elseif type == :problem1
        c = [10, 0.3, -20, 0.6, 0.3, 11, 0.4, 10, 0.5, 200, 0.1, 0.3, 1]
        for i in eachindex(ρ)
            ρ[i] = test_func(c, x[i])
        end
    elseif type == :problem2
        c = [200, 0.1, -200, 0.7, 0, 0, 0, 0, 0, 100, 0.3, 0.5, 0]
        for i in eachindex(ρ)
            ρ[i] = test_func(c, x[i])
        end
    elseif type == :single
        ρ[grid.nx÷2] = 1.0
    else
        ρ .= 1.0
    end
    u .= uconst

    return
end

"Cheating by remapping directly to the initial state."
function advance_SLICE_cheat!(
    grid::Grid1d,
    type::InterpolationMethod,
    nstep::Int,
    dt::Float64,
)
    ρinit = copy(grid.ρ)
    poly = PiecewisePolynomials(grid; type)

    for it = 1:nstep
        get_departure_points!(grid, dt * it)
        locate_cell_indexes!(grid)
        grid.ρ .= ρinit
        remap!(grid, poly; type)
    end
end

function advance_SLICE!(
    grid::Grid1d,
    type::InterpolationMethod,
    nstep::Int,
    dt::Float64,
    islimited::Bool = false,
)
    poly = PiecewisePolynomials(grid; type, islimited)
    isConstantAdvection = true
    if isConstantAdvection
        get_departure_points!(grid, dt)
    end
    for it = 1:nstep
        reconstruct!(grid, poly, islimited)
        if !isConstantAdvection
            get_departure_points!(grid, dt)
        end
        #scatter(grid.xn .+ grid.xs.*grid.dx, zeros(size(grid.xs)), marker="x", label="departure")
        locate_cell_indexes!(grid)
        remap!(grid, poly; type)
    end

    return
end

"Alternative classical semi-Lagrangian method."
function advance_SL!(grid::Grid1d, nstep::Int)
    (; nx, dx, ρ, u) = grid
    ρtemp = zeros(nx)
    for it = 1:nstep
        #@. grid.xs[1:end-1] = grid.x - grid.u*dt
        #interp = CubicSpline(grid.ρ, grid.x)
        #grid.ρ .= interp(@view grid.xs[1:end-1])
        # linear interpolation
        for i = 1:nx
            xs = -u[i] * dt / dx # departure point
            j = floor(Int, xs)
            α = xs - j

            ρtemp[i] = (1 - α) * ρ[i+j] + α * ρ[i+j+1]
        end
        ρ .= ρtemp
    end

    return
end

############
# number of physical cells in x
nx = 50
# constant advection speed
uconst = 1.0
# type of interpolations
type = pcom
islimited = true

grid = Grid1d(nx; xmin = 0.0, xmax = 1.0)
set_IC!(grid; type = :gaussian, uconst)

nstep = 30
CFL = 0.5
dt = CFL * grid.dx / uconst
#dt = (grid.xmax - grid.xmin) / nstep

@info "dt = $(dt)"
@info "CFL = $(uconst * dt / grid.dx)"

ρinit = copy(grid.ρ)
scatter(grid.x, ρinit, label = "cell center")
vlines(grid.xn, extrema(ρinit)..., colors = "gray", linestyles = "dashed")

# Currently we have issues at the right boundary!
xp = range(grid.xmin, grid.xmax - 1e-4, length = 401)
ap = zeros(size(xp))

for i in eachindex(xp)
    ap[i] = interpolate(xp[i], grid; type, islimited)
end
scatter(xp, ap, marker = ".", label = uppercase(string(type)))

advance_SLICE!(grid, type, nstep, dt, islimited)
#advance_SLICE_cheat!(grid, type, nstep, dt)
#advance_SL!(grid, nstep)

for i in eachindex(xp)
    ap[i] = interpolate(xp[i], grid; type, islimited)
end
scatter(xp, ap, marker = ".", label = uppercase(string(type)))

ishift = round(Int, nstep * dt * uconst / grid.dx)
circshift!(ρinit, ishift)
plot(grid.x, ρinit, label = "analytical")
scatter(grid.x, grid.ρ, label = "cell center new")

legend()

@info "initial mass = $(sum(ρinit))"
@info "relative mass diff = $(round((sum(ρinit) - sum(grid.ρ))/sum(ρinit)*100, digits=4))%"
@info "absolute mass diff = $(round(sum(ρinit) - sum(grid.ρ), digits=4))"
@info "L1 = $(norm(grid.ρ - ρinit, 1))"
@info "L2 = $(norm(grid.ρ - ρinit, 2))"
@info "L∞ = $(norm(grid.ρ - ρinit, Inf))"
