using WriteVTK

cd(@__DIR__)

x = 0:0.1:1
y = 0:0.2:1
z = -1:0.05:1

vtkfile = vtk_grid("my_vtk_file", x, y, z) # 3-D

outfiles = vtk_save(vtkfile)
