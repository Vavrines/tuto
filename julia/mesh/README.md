The support of meshing from current Julia packages aren't mature.

One alternative way is to do it in python with the help of PyCall.jl. This way, pure Python stack is preferred, e.g. meshio.
However, it lacks some key functionalities, e.g. computing connectivity.

PyMesh is more matured with its C++ basis. It's hard to be loaded from Julia, so we have to do it in python. After generating the PyObject, we write the arrays with numpy.save and numpy.savez, and read it into Julia via NPZ.jl.